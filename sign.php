<?php

require_once("../../config.php");
require_once("lib.php");
require_once("locallib.php");
require_once("classes/forms/sign_form.php");

require_login();

$context = context_system::instance();
require_capability('local/talentreview:form', $context);

$form_id = required_param('id', PARAM_INT);

$redirect_url = new moodle_url('/local/talentreview/');

$title = 'Talent Review Sign Form';

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('course');
$PAGE->set_heading($title);
$PAGE->set_title($title);
$PAGE->set_url(new moodle_url('/local/talentreview/sign.php'));

$PAGE->navbar->add(get_string('pluginname', 'local_talentreview'), new moodle_url('/local/talentreview/'));
$PAGE->navbar->add($title);

$PAGE->requires->jquery();
$PAGE->requires->css('/local/talentreview/style.css', true);

$form_data = $DB->get_record('local_talentreview', ['id' => $form_id]);

if(!LocalTalentReview::has_manager_access() && $form_data->user_id != $USER->id) {
    throw new required_capability_exception($context, 'local/talentreview:employess', 'Sorry, but you do not currently have permissions to do that', '');
}

$form = new sign_form(null, [
    'form_id' => $form_id,
    'data' => $form_data
]);
$form->set_data($form_data);
if ($form->is_cancelled()) {
    redirect($redirect_url);
} else if ($post = $form->get_data()) {

    $data = new stdClass();
    $data->id = $post->id;
    $data->signature = $post->signature;
    $data->signature_date = $post->signature_date;

    $DB->update_record('local_talentreview', $data);

    redirect(new moodle_url('/local/talentreview/', []));
    exit();
}

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('signform', 'local_talentreview'));

$form->display();

echo $OUTPUT->footer();
