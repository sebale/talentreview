<?php

global $CFG;
require_once($CFG->libdir . '/tablelib.php');

// In versions before Moodle 2.9 and 2.8, the supported callbacks have extends (not imperative mood) in their names. This was a consistency bug fixed in MDL-49643.
function local_talentreview_extends_navigation(global_navigation $nav){
    global $USER, $CFG;

    $context = context_system::instance();
    if(isloggedin() && has_capability('local/talentreview:index', $context)){
        if(!is_siteadmin()) {
            if (LocalTalentReview::has_manager_access()) {
                $nav->add(get_string('pluginname', 'local_talentreview'), new moodle_url($CFG->wwwroot . '/local/talentreview/employees.php'));
            } else {
                $nav->add(get_string('pluginname', 'local_talentreview'), new moodle_url($CFG->wwwroot . '/local/talentreview/index.php'));
            }
        }
    }
}

//call-back method to extend the navigation
function local_talentreview_extend_navigation(global_navigation $nav){
    global $USER, $CFG;

    $context = context_system::instance();
    if(isloggedin() && has_capability('local/talentreview:index', $context)){
        if(!is_siteadmin()) {
            if (LocalTalentReview::has_manager_access()) {
                $nav->add(get_string('pluginname', 'local_talentreview'), new moodle_url($CFG->wwwroot . '/local/talentreview/employees.php'));
            } else {
                $nav->add(get_string('pluginname', 'local_talentreview'), new moodle_url($CFG->wwwroot . '/local/talentreview/index.php'));
            }
        }
    }
}

function local_talentreview_extend_settings_navigation(settings_navigation $navigation) {
    global $PAGE;

    // Only add this settings item on non-site course pages.
    if (($PAGE->course && $PAGE->course->id != 1) || !has_capability('local/talentreview:index', context_system::instance())) {
        return;
    }

    // add admin settings navigation
    // ...
}

class LocalTalentReview {

    static $FORM_ORIGIN_ADMIN = 'admin';
    static $FORM_ORIGIN_SELF = 'self';
    static $FORM_ORIGIN_MANAGER = 'manager';

    static $SAVE_STATUS_DEFAULT = 0;
    static $SAVE_STATUS_SAVE = 1;
    static $SAVE_STATUS_SUBMIT = 2;

    static $SETTINGS_ROW_ID = 1;

    static function checkFormOrigin($user_id, $form) {
        if(is_siteadmin($user_id)) {
            return self::$FORM_ORIGIN_ADMIN;
        } elseif(LocalTalentReview::has_manager_access($user_id) && $form->user_id != $user_id) {
            return self::$FORM_ORIGIN_MANAGER;
        }
        return self::$FORM_ORIGIN_SELF;
    }

    static function hasAssignManager($user_id) {
        global $DB;
        if($record = $DB->get_record_sql('SELECT managerid FROM {pos_assignment} WHERE userid = ' . $user_id)) {
            return $record->managerid;
        }
        return false;
    }

    static function getAssignedUsersCount($user_id) {
        global $DB;
        return $DB->get_field_sql('SELECT COUNT(id) FROM {pos_assignment} WHERE managerid = ' . $user_id);
    }

    static function hasForm($user_id) {
        global $DB;
        if($record = $DB->get_record_sql('SELECT id FROM {local_talentreview} WHERE user_id = ' . $user_id . ' LIMIT 1')) {
            return $record->id;
        }
        return false;
    }

    static function get_user_roles_assignment($user_id) {
        global $DB;
        $list = $DB->get_records_sql('SELECT r.id as role_id, r.shortname as title
                  FROM {role_assignments} as ra
                  LEFT JOIN {role} as r ON ra.roleid = r.id
                  WHERE userid = :userid', array(
            'userid' => $user_id
        ));

        $def_role = new stdClass();
        $def_role->role_id = null;
        $def_role->title = 'user';
        return $list ? $list : array($def_role);
    }

    static function has_manager_access($user_id = null) {
        global $DB, $USER, $CFG;

        $result = true;

        $user_id = $user_id ? $user_id : $USER->id;
        if (!$DB->record_exists('pos_assignment', array('managerid' => $user_id))){
            if (!empty($CFG->enabletempmanagers)){
               $result = ($DB->record_exists('temporary_manager', array('tempmanagerid' => $user_id)));
            } else {
               $result = false;
            }
        }

        return $result;
    }

    static function hasNeedApproved($who_created, $whom_id) {
        if(self::has_manager_access($who_created)) {
            if($who_created != $whom_id) {
                return true;
            }
        }
        return false;
    }
}

function local_talentreview_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, $preview_param) {
    $relativepath = get_file_argument();

    // extract relative path components
    $args = explode('/', ltrim($relativepath, '/'));
    $record_id = (int)array_pop($args);
    $fs = get_file_storage();
    if(is_int($record_id) && $file = $fs->get_file_by_id($record_id)) {

        \core\session\manager::write_close();
        send_stored_file($file, 60*60, 0, $forcedownload, $preview_param);
    }
}

function local_talentreview_get_overal_annual($form) {
    $overal = array(
        'behaviors' => array(
            'rating' => $form->behaviors_overal,
            'bal' => null
        ),
        'perfomance' => array(
            'rating' => $form->goals_perfomance_overal,
            'bal' => null
        )
    );

    foreach ($overal as &$item) {
        if($item['rating'] == "N") {
            $item['bal'] = 2;
        }
        if($item['rating'] == "A") {
            $item['bal'] = 3;
        }
        if($item['rating'] == "E") {
            $item['bal'] = 5;
        }
    }
    return abs(($overal['behaviors']['bal'] + $overal['perfomance']['bal'])/2);
}
