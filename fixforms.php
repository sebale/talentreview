<?php

require_once("../../config.php");

require_login();

// admin redirect to managers page
if(!is_siteadmin()) {
    redirect(new moodle_url("/local/talentreview/index.php"));
}

$sql = "SELECT f.id, f.name, f.user_id, u.firstname, u.lastname
          FROM {local_talentreview} f
     LEFT JOIN {user} u ON u.id = f.user_id
         WHERE CONCAT(u.firstname, ' ', u.lastname) <> f.name AND u.id > 1";
$forms = $DB->get_records_sql($sql);

if (count($forms)) {
    foreach ($forms as $form) {
        $user = $DB->get_record_sql("SELECT * FROM {user} WHERE CONCAT(firstname, ' ', lastname) = '".$form->name."'");
        if (isset($user->id)) {
            $upd = new stdClass();
            $upd->id = $form->id;
            $upd->user_id = $user->id;
            $DB->update_record('local_talentreview', $upd, true);
            var_dump($form); echo '<hr />';
        }
    }
}

exit;
