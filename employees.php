<?php

require_once("../../config.php");
require_once("lib.php");
require_once("locallib.php");
require_once("classes/forms/talentreview_create_manager_form.php");
require_once("classes/forms/talentreview_search_user_form.php");
require_once("classes/tables/employers_table.php");

require_login();

// page use only managers
$context = context_system::instance();
if(!LocalTalentReview::has_manager_access()) {
    throw new required_capability_exception($context, 'local/talentreview:employess', 'nopermissions', '');
}

// admin redirect to managers page
if(is_siteadmin()) {
    redirect(new moodle_url("/local/talentreview/managers.php"));
}

$form_id = optional_param('id', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);
$search = optional_param('search', '', PARAM_RAW);
$print = optional_param('print', 0, PARAM_INT);
$statusfilter = optional_param('statusfilter', 2, PARAM_INT);

if ($print) {
    local_talentreview_bulk_print();
}

switch ($action) {
    case 'delete':
        $DB->delete_records('local_talentreview', ['id' => $form_id]);
        redirect(new moodle_url('/local/talentreview/'));
        break;
}
if ($statusfilter < 2) {
    set_user_preference('statusfilter', $statusfilter);
}

$title = 'Employees';
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('course');
$PAGE->set_url(new moodle_url("/local/talentreview/employees.php"));
$PAGE->set_heading($title);
$PAGE->set_title($title);

$PAGE->navbar->add(get_string('pluginname', 'local_talentreview'), new moodle_url('/local/talentreview/'));
$PAGE->navbar->add($title);

$PAGE->requires->jquery();
$PAGE->requires->css('/local/talentreview/style.css', true);

$table = new employers_table('employers_table');
$table->show_download_buttons_at([]);
$table->is_collapsible = false;

$form = new talentreview_create_manager_form($CFG->wwwroot . '/local/talentreview/form.php?id=0');

$editform = new talentreview_search_user_form(null, array('print'=>'1'));
$editform->set_data(array('search' => $search));
if ($editform->is_cancelled()) {
    redirect(new moodle_url('/local/talentreview/employees.php'));
} else if ($data = $editform->get_data()) {
    redirect(new moodle_url('/local/talentreview/employees.php', array('search' => $data->search)));
}

echo $OUTPUT->header();
?>

    <div class="controls controls-row">
        <div class="span6">
            <?php echo $OUTPUT->heading($title); ?>
        </div>

        <div class="span6">
            <?php if(LocalTalentReview::hasForm($USER->id)): ?>
                <a href="<?php echo $CFG->wwwroot . '/local/talentreview/index.php' ?>" class="btn pull-right my-form" title="Manage my form"> Manage My Form <!--<img src="<?php /*echo $CFG->wwwroot . '/local/talentreview/assets/img/hamburger.png' */?>" height="15"> --></a>
            <?php endif; ?>

            <?php if(LocalTalentReview::hasAssignManager($USER->id)): ?>
                <a href="<?php echo $CFG->wwwroot . '/local/talentreview/form.php?id=0' ?>" class="btn pull-right"> Self Review </a>
            <?php endif; ?>
        </div>
    </div>
    <div class="controls controls-row">
        <div class="span12">
            <div class="span4 pull-left">
                <?php echo $form->display(); ?>
            </div>
            <div class="span8 pull-right">
                <?php echo $editform->display(); ?>
            </div>
        </div>
    </div>
    <?php echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'method'=>'POST', 'target'=>'_blank', 'id'=>'bulk_print_form')); ?>
    <?php echo html_writer::empty_tag("input",  array('name'=>'print', 'type'=>'hidden', 'value'=>1)); ?>
    <?php $table->out(30, true); ?>
    <?php echo html_writer::end_tag("form"); ?>
<?php
echo $OUTPUT->footer();
