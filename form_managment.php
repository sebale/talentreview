<?php

require_once("../../config.php");
require_once("lib.php");
require_once("locallib.php");
require_once("classes/forms/talentreview_search_user_form.php");
require_once("classes/tables/talentreview_form_managment_table.php");

require_login();

// admin redirect to managers page
if(!is_siteadmin()) {
    redirect(new moodle_url("/local/talentreview/"));
}

$search = optional_param('search', '', PARAM_RAW);
$statusfilter = optional_param('statusfilter', 2, PARAM_INT);
$print = optional_param('print', 0, PARAM_INT);

if ($statusfilter < 2) {
    set_user_preference('statusfilter', $statusfilter);
}

if ($print) {
    local_talentreview_bulk_print();
}

$title = 'Form Managment';

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('course');
$PAGE->set_heading($title);
$PAGE->set_title($title);
$PAGE->set_url(new moodle_url('/local/talentreview/form_managment.php'));

$PAGE->navbar->add(get_string('pluginname', 'local_talentreview'), new moodle_url('/local/talentreview/'));
$PAGE->navbar->add($title);

$PAGE->requires->jquery();
$PAGE->requires->css('/local/talentreview/style.css', true);

$table = new talentreview_form_managment_table('talentreview_form_managment_table', array('search' => $search));
$table->show_download_buttons_at([]);
$table->is_collapsible = false;
$table->no_sorting('actions');

$editform = new talentreview_search_user_form(null, array('print'=>'1'));
$editform->set_data(array('search' => $search));
if ($editform->is_cancelled()) {
    redirect(new moodle_url('/local/talentreview/form_managment.php'));
} else if ($data = $editform->get_data()) {
    redirect(new moodle_url('/local/talentreview/form_managment.php', array('search' => $data->search)));
}

echo $OUTPUT->header();
?>

    <?php echo $OUTPUT->heading($title); ?>

    <div class="controls controls-row clearfix">
        <div class="btn-group pull-left">
            <?php echo $editform->display(); ?>
        </div>
    </div>

    <?php echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'method'=>'POST', 'target'=>'_blank', 'id'=>'bulk_print_form')); ?>
    <?php echo html_writer::empty_tag("input",  array('name'=>'print', 'type'=>'hidden', 'value'=>1)); ?>
    <?php $table->out(50, true); ?>
    <?php echo html_writer::end_tag("form"); ?>

<?php
echo $OUTPUT->footer();
