<?php

if (!function_exists('pre')) {
    function pre($data, $exit = true) {
        echo "<pre>";
            print_r($data);
        echo "</pre>";

        if($exit)
            exit();
    }
}

function talentreview_update_picture($data, $userform) {
    global $CFG, $DB;
    require_once("$CFG->libdir/gdlib.php");

    $context = context_user::instance($data->user_id, MUST_EXIST);
    $talentreview = $DB->get_record('local_talentreview', array('id' => $data->id), 'id, photo', MUST_EXIST);

    $newpicture = $talentreview->photo;
    // Get file_storage to process files.
    $fs = get_file_storage();

    // Save newly uploaded file, this will avoid context mismatch for newly created users.
    file_save_draft_area_files($data->imagefile, $context->id, 'local_talentreview', 'temp_img', $talentreview->id);
    if (($iconfiles = $fs->get_area_files($context->id, 'local_talentreview', 'temp_img', $talentreview->id)) && count($iconfiles) == 2) {

        // Get file which was uploaded in draft area.
        foreach ($iconfiles as $file) {
            if (!$file->is_directory()) {
                break;
            }
        }

        // Copy file to temporary location and the send it for processing icon.
        if ($iconfile = $file->copy_content_to_temp()) {
            // There is a new image that has been uploaded.
            // Process the new image and set the user to make use of it.
            // NOTE: Uploaded images always take over Gravatar.
            $newpicture = (int)process_new_icon($context, 'local_talentreview', 'img', 0, $iconfile);
            // Delete temporary file.
            @unlink($iconfile);
            // Remove uploaded file.
            $fs->delete_area_files($context->id, 'local_talentreview', 'temp_img');
        } else {
            // Something went wrong while creating temp file.
            // Remove uploaded file.
            $fs->delete_area_files($context->id, 'local_talentreview', 'temp_img');
            return false;
        }
        //$newpicture = $file->get_id();
    }

    $id = ($newpicture != $talentreview->photo) ? $newpicture : $talentreview->photo;
    $DB->set_field('local_talentreview', 'photo', $id, ['id' => $talentreview->id]);
}

function local_talentreview_bulk_print() {
    global $CFG, $DB, $USER;

    if (!count($_POST['forms'])) {
        return false;
    }
    $forms = array_keys($_POST['forms']);

    require_once($CFG->libdir . '/pdflib.php');
    //TCPDF_FONTS::addTTFfont('/full_path_to/ARIALUNI.TTF', 'TrueTypeUnicode');

    // **************************************************************
    // generate PDF

    $pdf = new pdf('L', 'pt');
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    foreach ($forms as $form_id) {
        $form = $DB->get_record('local_talentreview', ['id' => $form_id]);
        if(!$form->id) {
            continue;
        }

        $fs = get_file_storage();
        $profile_img = '';
        if($profile_img_file = $fs->get_file_by_id($form->photo)) {
            require_once("$CFG->libdir/filelib.php");

            if (isset($CFG->filedir)) {
                $filedir = $CFG->filedir;
            } else {
                $filedir = $CFG->dataroot.'/filedir';
            }

            $profile_img_file->sync_external_file();

            // Detect is local file or not.
            $contenthash = $profile_img_file->get_contenthash();
            $l1 = $contenthash[0].$contenthash[1];
            $l2 = $contenthash[2].$contenthash[3];
            $profile_img = "{$filedir}/$l1/$l2/$contenthash";
        }



        // generate page
        local_talentreview_print_page($form, $pdf, $profile_img);

    }

    // Render PDF
    $pdf->Output('talentreview.pdf');

}

function local_talentreview_print_page($form = null, &$pdf = null, $profile_img = '') {
    global $CFG, $DB, $USER;

    $pdf->AddPage();
    $pdf->SetDrawColor(128, 128 ,128);

    $padding = 15;
    $padding_entry = 8;
    $height = 8;

    // ***************************************************************
    // row 1 header
    /*$pdf->SetXY($padding - 8, $height);
    $pdf->SetFont('freesans', '', 20);
    $pdf->Cell(600, 10, 'Talent Review Assessment', 0, 0, 'L');
    $width = $padding + 600 + 35;

    $top_magrin = 5;
    $pdf->SetXY($width, $height + $top_magrin);
    $pdf->SetFont('freesans', '', 10);
    $pdf->Cell(70, 10, 'Self Review', 0, 0, 'L');
    $width += 60;

    $pdf->SetXY($width, $height + $top_magrin);
    $pdf->SetFont('freesans', '', 10);
    $pdf->Cell(12, 10, ($form->form_origin == LocalTalentReview::$FORM_ORIGIN_SELF ? 'x' : ''), 1, 0, 'C');
    $width += 20;

    $pdf->SetXY($width, $height + $top_magrin);
    $pdf->SetFont('freesans', '', 10);
    $pdf->Cell(95, 10, 'Manager Review ', 0, 0, 'L');
    $width += 82;

    $pdf->SetXY($width, $height + $top_magrin);
    $pdf->SetFont('freesans', '', 10);
    $pdf->Cell(12, 10, ($form->form_origin == LocalTalentReview::$FORM_ORIGIN_MANAGER ? 'x' : ''), 1, 0, 'C');
    $width = $padding;
    $height += 28;*/

    // ***************************************************************
    // row 2
    // use $custom_data

    $pdf->SetXY($padding, $height);
    $block_height = 100;

    $photo_block_width = $photo_block_height = 60;
    $pdf->SetFillColor(0, 114, 206);
    $pdf->Cell($photo_block_width, $block_height, '', 1, 0, 'C', true);

    $pdf->SetXY($padding, $height+20);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Cell($photo_block_height, $photo_block_height, '', 0, 0, 'C', true);
    if (!$profile_img) {
        $pdf->SetXY($padding+14, $height+18);
        $pdf->Cell($padding, $photo_block_height-15, 'Insert', 0, 0, 'L', 0);

        $pdf->SetXY($padding+14, $height+18);
        $pdf->Cell($padding, $photo_block_height+15, 'photo', 0, 0, 'L', 0);
    }

    if($profile_img) {
        $pdf->Image($profile_img, $padding, $height+20, $photo_block_width, $photo_block_width);
    }

    $width += $photo_block_height+15;
    $text_height = 20;
    $text_width = 140;
    $label_width = 80;

    $pdf->SetXY($width, $height);
    $pdf->Cell(500, $block_height, '', 1, 0, 'C', 0);
    $row_width = $width + 5;

    $pdf->SetXY($row_width, $height);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Cell($label_width, $text_height, 'Name:', 0, 0, 'L', 0);
    $pdf->SetXY($row_width+$label_width, $height);
    $pdf->Cell($text_width, $text_height, $form->name, 0, 0, 'L', 0);
    $row_height = $height + $text_height;

    $pdf->SetXY($row_width, $row_height);
    $pdf->Cell($label_width, $text_height, 'Title:', 0, 0, 'L', 0);
    $pdf->SetXY($row_width+$label_width, $row_height);
    $pdf->Cell($text_width, $text_height, $form->title, 0, 0, 'L', 0);
    $row_height += $text_height;

    $pdf->SetXY($row_width, $row_height);
    $pdf->Cell($label_width, $text_height, 'BU/Function:', 0, 0, 'L', 0);
    $pdf->SetXY($row_width+$label_width, $row_height);
    $pdf->Cell($text_width, $text_height, $form->bu_function, 0, 0, 'L', 0);
    $row_height += $text_height;

    $pdf->SetXY($row_width, $row_height);
    $pdf->Cell($label_width, $text_height, 'Manager:', 0, 0, 'L', 0);
    $pdf->SetXY($row_width+$label_width, $row_height);
    $pdf->Cell($text_width, $text_height, $form->manager, 0, 0, 'L', 0);
    $row_height += $text_height;

    $pdf->SetXY($row_width, $row_height);
    $pdf->Cell($label_width, $text_height, 'Time in Role:', 0, 0, 'L', 0);
    $pdf->SetXY($row_width+$label_width, $row_height);
    $pdf->Cell($text_width, $text_height, ((!empty(local_talentreview_calculate_timeinrole($form->svmhiredate))) ? local_talentreview_calculate_timeinrole($form->svmhiredate) : ''), 0, 0, 'L', 0);

    $row_width = $text_width + $text_width + $text_width/4;
    $label_width = 100;

    $pdf->SetXY($row_width, $height);
    $pdf->Cell($text_width, $text_height, get_string('svmhiredate', 'local_talentreview').':', 0, 0, 'L', 0);
    $pdf->SetXY($row_width+$label_width, $height);
    $pdf->Cell($text_width, $text_height, (($form->svmhiredate) ? date('m-d-Y', $form->svmhiredate) : ''), 0, 0, 'L', 0);
    $row_height = $height + $text_height;

    $pdf->SetXY($row_width, $row_height);
    $pdf->Cell($text_width, $text_height, get_string('degree', 'local_talentreview').':', 0, 0, 'L', 0);
    $pdf->SetXY($row_width+$label_width, $row_height);
    $pdf->Cell($text_width, $text_height, $form->degree, 0, 0, 'L', 0);
    $row_height += $text_height;

    $pdf->SetXY($row_width, $row_height);
    $pdf->Cell($text_width, $text_height, get_string('university_school', 'local_talentreview').':', 0, 0, 'L', 0);
    $pdf->SetXY($row_width+$label_width, $row_height);
    $pdf->Cell($text_width, $text_height, $form->university_school, 0, 0, 'L', 0);
    $row_height += $text_height;

    $pdf->SetXY($row_width, $row_height);
    $pdf->Cell($text_width, $text_height, get_string('previous_work', 'local_talentreview').':', 0, 0, 'L', 0);
    $pdf->SetXY($row_width+$label_width, $row_height);
    $pdf->Cell($text_width, $text_height, (strlen($form->previous_employer) > 25) ? substr($form->previous_employer, 0, 25) : $form->previous_employer, 0, 0, 'L', 0);
    $row_height += $text_height;

    $pdf->SetXY($row_width, $row_height);
    $pdf->Cell($text_width, $text_height, get_string('review_year', 'local_talentreview').':', 0, 0, 'L', 0);
    $pdf->SetXY($row_width+$label_width, $row_height);
    $pdf->Cell($text_width, $text_height, $form->review_year, 0, 0, 'L', 0);
    $row_height += $text_height;

    $row_width += $text_width;
    $width += 500;

    $pdf->StartTransform();
    $pdf->rotate(90);
    //$pdf->show_xy("This is horizontal text",50, 300);
    $row_height = $row_height + 2;
    $pdf->SetXY($width-30, $height+90);
    $pdf->SetFont('freesans', 'n', 9);
    $pdf->Cell(50, 10, 'Performance', false);
    $pdf->StopTransform();

    $row_height = $height;

    $pdf->SetXY($width, $height);
    $pdf->SetFillColor(0, 114, 206);
    $pdf->SetTextColor(255, 255, 255);
    $pdf->SetFont('freesans', 'b', 11);
    $pdf->Cell(140, $text_height, '9-Box Placement', 1, 0, 'C', true);

    // 9 box placement
    $row_height = $height + $text_height;
    $pdf->SetTextColor(0, 0, 0);

    $pdf->SetXY($width, $row_height);
    $pdf->Cell(140, 80, '', 1, 0, 'C', false);
    $row_height += 2;

    $pdf->SetXY($width+10, $row_height);
    $pdf->SetFont('freesans', 'n', 9);
    $pdf->Cell(140, 10, 'Behaviors', false, 0, 'C', false);

    $row_width = $width;

    $row_width += 20;
    $pdf->SetXY($row_width + 30, $row_height + 9);
    $pdf->SetFont('freesans', '', 8);
    $pdf->Cell(20, 15, 'E', false, 0, 'C', false);

    $pdf->SetXY($row_width + 50, $row_height + 9);
    $pdf->SetFont('freesans', '', 8);
    $pdf->Cell(20, 15, 'A', false, 0, 'C', false);

    $pdf->SetXY($row_width + 70, $row_height + 9);
    $pdf->SetFont('freesans', '', 8);
    $pdf->Cell(20, 15, 'N', false, 0, 'C', false);

    // box 3*3
    $pdf->SetXY($row_width + 30, $row_height + 22);
    $pdf->SetFont('freesans', '', 9);
    $pdf->Cell(20, 15, (($form->behaviors_overal == "E" && $form->goals_perfomance_overal == "E") ? 'X' : ''), true, 0, 'C', false);

    $pdf->SetXY($row_width + 50, $row_height + 22);
    $pdf->SetFont('freesans', '', 9);
    $pdf->Cell(20, 15, (($form->behaviors_overal == 'A' && $form->goals_perfomance_overal == 'E') ? 'X' : ''), true, 0, 'C', false);

    $pdf->SetXY($row_width + 70, $row_height + 22);
    $pdf->SetFont('freesans', '', 9);
    $pdf->Cell(20, 15, (($form->behaviors_overal == 'N' && $form->goals_perfomance_overal == 'E') ? 'X' : ''), true, 0, 'C', false);

    $pdf->SetXY($row_width + 30, $row_height + 37);
    $pdf->SetFont('freesans', '', 9);
    $pdf->Cell(20, 15, (($form->behaviors_overal == 'E' && $form->goals_perfomance_overal == 'A') ? 'X' : ''), true, 0, 'C', false);

    $pdf->SetXY($row_width + 50, $row_height + 37);
    $pdf->SetFont('freesans', '', 9);
    $pdf->Cell(20, 15, (($form->behaviors_overal == 'A' && $form->goals_perfomance_overal == 'A') ? 'X' : ''), true, 0, 'C', false);

    $pdf->SetXY($row_width + 70, $row_height + 37);
    $pdf->SetFont('freesans', '', 9);
    $pdf->Cell(20, 15, (($form->behaviors_overal == 'N' && $form->goals_perfomance_overal == 'A') ? 'X' : ''), true, 0, 'C', false);

    $pdf->SetXY($row_width + 30, $row_height + 52);
    $pdf->SetFont('freesans', '', 9);
    $pdf->Cell(20, 15, (($form->behaviors_overal == 'E' && $form->goals_perfomance_overal == 'N') ? 'X' : ''), true, 0, 'C', false);

    $pdf->SetXY($row_width + 50, $row_height + 52);
    $pdf->SetFont('freesans', '', 9);
    $pdf->Cell(20, 15, (($form->behaviors_overal == 'A' && $form->goals_perfomance_overal == 'N') ? 'X' : ''), true, 0, 'C', false);

    $pdf->SetXY($row_width + 70, $row_height + 52);
    $pdf->SetFont('freesans', '', 9);
    $pdf->Cell(20, 15, (($form->behaviors_overal == 'N' && $form->goals_perfomance_overal == 'N') ? 'X' : ''), true, 0, 'C', false);
    // end box 3*3


    $pdf->StartTransform();
    $pdf->rotate(90);
    $h = 22; $w = 45;
    $pdf->SetXY($row_width + 25 + $w, $row_height - $h);
    $pdf->SetFont('freesans', '', 8);
    $pdf->Cell(20, 15, 'N', false, 0, 'C', false);

    $pdf->SetXY($row_width + 42 + $w, $row_height - $h);
    $pdf->SetFont('freesans', '', 8);
    $pdf->Cell(20, 15, 'A', false, 0, 'C', false);

    $pdf->SetXY($row_width + 58 + $w, $row_height - $h);
    $pdf->SetFont('freesans', '', 8);
    $pdf->Cell(20, 15, 'E', false, 0, 'C', false);
    $pdf->StopTransform();
    // end 9 box placement

    $width += 140;
    $pdf->SetXY($width, $height);
    $pdf->SetFillColor(0, 114, 206);
    $pdf->SetTextColor(255, 255, 255);
    $pdf->SetFont('freesans', 'b', 11);
    $pdf->Cell(110, $text_height, 'Overall Rating', 1, 0, 'C', true);

    $pdf->SetXY($width, $row_height-2);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Cell(110, 80, '', 1, 0, 'C', false);
    $row_height += 4;

    $row_width = $width;
    $pdf->SetXY($row_width, $row_height);
    $pdf->SetFont('freesans', 'b', 11);
    $pdf->Cell(60, 15, 'Current', false, 0, 'C', false);
    $row_height += 15;

    $pdf->SetXY($row_width, $row_height);
    $pdf->SetFont('freesans', 'b', 11);
    $pdf->Cell(60, 15, 'Year', false, 0, 'C', false);

    $row_height += 5;
    $pdf->SetXY($row_width+56, $row_height-14);
    $pdf->SetFont('freesans', '', 14);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Cell(50, 15, ($form->overal_review_rating ? $form->overal_review_rating : ''), 0, 0, 'C', 0);

    $row_width = $width;
    $row_height += 15;
    $pdf->SetXY($row_width, $row_height);
    $pdf->Cell(110, 39, '', 1, 0, 'C', false);
    $row_height += 4;

    $pdf->SetXY($row_width, $row_height);
    $pdf->SetFont('freesans', 'n', 11);
    $pdf->Cell(60, 15, 'Previous', false, 0, 'C', false);
    $row_height += 15;

    $pdf->SetXY($row_width, $row_height);
    $pdf->SetFont('freesans', 'n', 11);
    $pdf->Cell(60, 15, 'Year', false, 0, 'C', false);

    $row_height += 5;
    $pdf->SetXY($row_width+56, $row_height-12);
    $pdf->SetFont('freesans', '', 12);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Cell(50, 15, ($form->overal_review_rating_previous ? $form->overal_review_rating_previous : ''), 0, 0, 'C', 0);

    $width = $padding;
    $height += $block_height;
    // ***************************************************************
    // separator
    $row_height = $height;
    $pdf->SetTextColor(255, 255, 255);
    $pdf->SetFont('freesans', 'b', 11);
    $pdf->SetFillColor(0, 114, 206);

    $pdf->SetXY($width, $height);
    $pdf->Cell(180, 20, 'Goals and Results', 1, 0, 'L', true);
    $width += 180;

    $pdf->SetXY($width, $height);
    $pdf->Cell(220, 20, 'Performance Rating', 1, 0, 'L', true);
    $width += 170;

    $row_height += 12;
    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $width - 30, $row_height, 25, 5);
    $pdf->SetXY($width - 30, $row_height - 9);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->Cell(25, 12, $form->goals_perfomance_overal, false, 0, 'C', true);

    $width = $padding;
    $height += $text_height;

    // ***************************************************************
    // start goals
    $row_height = $height;
    $goals_width = 350;
    $goals_height = 100;

    // goal 1
    $pdf->SetXY($width, $row_height);
    $pdf->Cell($goals_width, $goals_height, '', 1, 0, 'C', false);

    $pdf->SetXY($width, $row_height);
    $pdf->SetFont('freesans', 'b', 10);
    $pdf->Cell($goals_width, $text_height, 'Goal 1:', 0, 0, 'L', false);

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $goals_width - 15, $row_height+12, 25, 5);
    $pdf->SetXY($goals_width - 15, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Cell(25, 10, $form->goals_rating, false, 0, 'C', false);

    $row_height += $text_height;
    $pdf->SetXY($width, $row_height);
    $pdf->SetFont('freesans', '', 9);
    $conf = form_conf('goals_comment');
    $pdf->MultiCell($goals_width, $goals_height-$text_height, substr($form->goals_comment, 0, $conf['num_chars']), 0, 'L');
    $pdf->Ln();

    // goal 2
    $row_height += $goals_height-$text_height;
    $pdf->SetXY($width, $row_height);
    $pdf->Cell($goals_width, $goals_height, '', 1, 0, 'C', false);

    $pdf->SetXY($width, $row_height);
    $pdf->SetFont('freesans', 'b', 10);
    $pdf->Cell($goals_width, $text_height, 'Goal 2:', 0, 0, 'L', false);

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $goals_width - 15, $row_height+12, 25, 5);
    $pdf->SetXY($goals_width - 15, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Cell(25, 10, $form->goal2_rating, false, 0, 'C', false);

    $row_height += $text_height;
    $pdf->SetXY($width, $row_height);
    $pdf->SetFont('freesans', '', 9);
    $pdf->MultiCell($goals_width, $goals_height-$text_height, substr($form->goal2_comment, 0, $conf['num_chars']), 0, 'L');
    $pdf->Ln();

    // goal 3
    $row_height += $goals_height-$text_height;
    $pdf->SetXY($width, $row_height);
    $pdf->Cell($goals_width, $goals_height, '', 1, 0, 'C', false);

    $pdf->SetXY($width, $row_height);
    $pdf->SetFont('freesans', 'b', 10);
    $pdf->Cell($goals_width, $text_height, 'Goal 3:', 0, 0, 'L', false);

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $goals_width - 15, $row_height+12, 25, 5);
    $pdf->SetXY($goals_width - 15, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Cell(25, 10, $form->goal3_rating, false, 0, 'C', false);

    $row_height += $text_height;
    $pdf->SetXY($width, $row_height);
    $pdf->SetFont('freesans', '', 9);
    $pdf->MultiCell($goals_width, $goals_height-$text_height, substr($form->goal3_comment, 0, $conf['num_chars']), 0, 'L');
    $pdf->Ln();

    // goal 4
    $row_height += $goals_height-$text_height;
    $pdf->SetXY($width, $row_height);
    $pdf->Cell($goals_width, $goals_height, '', 1, 0, 'C', false);

    $pdf->SetXY($width, $row_height);
    $pdf->SetFont('freesans', 'b', 10);
    $pdf->Cell($goals_width, $text_height, 'Goal 4:', 0, 0, 'L', false);

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $goals_width - 15, $row_height+12, 25, 5);
    $pdf->SetXY($goals_width - 15, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Cell(25, 10, $form->goal4_rating, false, 0, 'C', false);

    $row_height += $text_height;
    $pdf->SetXY($width, $row_height);
    $pdf->SetFont('freesans', '', 9);
    $pdf->MultiCell($goals_width, $goals_height-$text_height, substr($form->goal4_comment, 0, $conf['num_chars']), 0, 'L');
    $pdf->Ln();

    // Behaviors Rating
    $pdf->SetTextColor(255, 255, 255);
    $pdf->SetFont('freesans', 'b', 11);
    $pdf->SetFillColor(0, 114, 206);

    $width += $goals_width;
    $height -= $text_height;
    $row_height = $height;
    $behaviorsbox_width = 150;
    $behavior_width = 120;

    $pdf->SetXY($width, $height);
    $pdf->Cell($behaviorsbox_width, 20, 'Behaviors Rating', 1, 0, 'L', true);

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $width+$behaviorsbox_width - 30, $row_height+12, 25, 5);
    $pdf->SetXY($width + $behaviorsbox_width - 30, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->Cell(25, 12, $form->behaviors_overal, false, 0, 'C', true);

    // ----------------- //
    $row_height += $text_height;
    $pdf->SetXY($width, $row_height+2);
    $pdf->SetFont('freesans', '', 11);
    $pdf->MultiCell($behavior_width, $text_height+10, 'Growth & Customer Focus', 0, 'L');
    $pdf->Ln();

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $width+$behaviorsbox_width - 30, $row_height+15, 25, 5);
    $pdf->SetXY($width + $behaviorsbox_width - 30, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->Cell(25, 12, $form->behaviors_growth, false, 0, 'C', true);
    // ----------------- //
    $row_height += $text_height+10;
    $pdf->SetXY($width, $row_height+2);
    $pdf->SetFont('freesans', '', 11);
    $pdf->MultiCell($behavior_width, $text_height, 'Accountability', 0, 'L');
    $pdf->Ln();

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $width+$behaviorsbox_width - 30, $row_height+12, 25, 5);
    $pdf->SetXY($width + $behaviorsbox_width - 30, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->Cell(25, 12, $form->behaviors_accountability, false, 0, 'C', true);
    // ----------------- //
    $row_height += $text_height;
    $pdf->SetXY($width, $row_height+2);
    $pdf->SetFont('freesans', '', 11);
    $pdf->MultiCell($behavior_width, $text_height, 'Self Aware/Learner', 0, 'L');
    $pdf->Ln();

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $width+$behaviorsbox_width - 30, $row_height+12, 25, 5);
    $pdf->SetXY($width + $behaviorsbox_width - 30, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->Cell(25, 12, $form->behaviors_self_aware, false, 0, 'C', true);
    // ----------------- //
    $row_height += $text_height;
    $pdf->SetXY($width, $row_height+2);
    $pdf->SetFont('freesans', '', 11);
    $pdf->MultiCell($behavior_width, $text_height, 'Champions Change', 0, 'L');
    $pdf->Ln();

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $width+$behaviorsbox_width - 30, $row_height+12, 25, 5);
    $pdf->SetXY($width + $behaviorsbox_width - 30, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->Cell(25, 12, $form->behaviors_champions, false, 0, 'C', true);
    // ----------------- //
    $row_height += $text_height;
    $pdf->SetXY($width, $row_height+2);
    $pdf->SetFont('freesans', '', 11);
    $pdf->MultiCell($behavior_width, $text_height, 'Initiative', 0, 'L');
    $pdf->Ln();

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $width+$behaviorsbox_width - 30, $row_height+12, 25, 5);
    $pdf->SetXY($width + $behaviorsbox_width - 30, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->Cell(25, 12, $form->behaviors_initiative, false, 0, 'C', true);
    // ----------------- //
    $row_height += $text_height;
    $pdf->SetXY($width, $row_height+2);
    $pdf->SetFont('freesans', '', 11);
    $pdf->MultiCell($behavior_width, $text_height, 'Judgment', 0, 'L');
    $pdf->Ln();

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $width+$behaviorsbox_width - 30, $row_height+12, 25, 5);
    $pdf->SetXY($width + $behaviorsbox_width - 30, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->Cell(25, 12, $form->behaviors_judgment, false, 0, 'C', true);
    // ----------------- //
    $row_height += $text_height;
    $pdf->SetXY($width, $row_height+2);
    $pdf->SetFont('freesans', '', 11);
    $pdf->MultiCell($behavior_width, $text_height, 'Makes People Better', 0, 'L');
    $pdf->Ln();

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $width+$behaviorsbox_width - 30, $row_height+12, 25, 5);
    $pdf->SetXY($width + $behaviorsbox_width - 30, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->Cell(25, 12, $form->behaviors_makes_people, false, 0, 'C', true);
    // ----------------- //
    $row_height += $text_height;
    $pdf->SetXY($width, $row_height+2);
    $pdf->SetFont('freesans', '', 11);
    $pdf->MultiCell($behavior_width, $text_height, 'Leadership Impact', 0, 'L');
    $pdf->Ln();

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $width+$behaviorsbox_width - 30, $row_height+12, 25, 5);
    $pdf->SetXY($width + $behaviorsbox_width - 30, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->Cell(25, 12, $form->behaviors_leadership, false, 0, 'C', true);
    // ----------------- //
    $row_height += $text_height;
    $pdf->SetXY($width, $row_height+2);
    $pdf->SetFont('freesans', '', 11);
    $pdf->MultiCell($behavior_width, $text_height, 'Effective Communicator', 0, 'L');
    $pdf->Ln();

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $width+$behaviorsbox_width - 30, $row_height+12, 25, 5);
    $pdf->SetXY($width + $behaviorsbox_width - 30, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->Cell(25, 12, $form->behaviors_effective_com, false, 0, 'C', true);
    // ----------------- //
    $row_height += $text_height;
    $pdf->SetXY($width, $row_height+2);
    $pdf->SetFont('freesans', '', 11);
    $pdf->MultiCell($behavior_width, $text_height, 'Gets Results', 0, 'L');
    $pdf->Ln();

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $width+$behaviorsbox_width - 30, $row_height+12, 25, 5);
    $pdf->SetXY($width + $behaviorsbox_width - 30, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->Cell(25, 12, $form->behaviors_gets_result, false, 0, 'C', true);
    // ----------------- //
    $row_height += $text_height;
    $pdf->SetXY($width, $row_height+2);
    $pdf->SetFont('freesans', '', 11);
    $pdf->MultiCell($behavior_width, $text_height, 'Integrative Thinker', 0, 'L');
    $pdf->Ln();

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $width+$behaviorsbox_width - 30, $row_height+12, 25, 5);
    $pdf->SetXY($width + $behaviorsbox_width - 30, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->Cell(25, 12, $form->behaviors_integrative, false, 0, 'C', true);
    // ----------------- //
    $row_height += $text_height;
    $pdf->SetXY($width, $row_height+2);
    $pdf->SetFont('freesans', '', 11);
    $pdf->MultiCell($behavior_width, $text_height, 'Intelligent Risk Taker', 0, 'L');
    $pdf->Ln();

    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/border_bottom.jpg', $width+$behaviorsbox_width - 30, $row_height+12, 25, 5);
    $pdf->SetXY($width + $behaviorsbox_width - 30, $row_height+3);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFillColor(255, 255, 255);
    $pdf->Cell(25, 12, $form->behaviors_intelligent, false, 0, 'C', true);
    // ----------------- //

    $row_height += $text_height+10;
    $pdf->SetXY($width, $row_height);
    $pdf->Cell($behaviorsbox_width, 140, '', 1, 0, 'C', false);

    $pdf->SetXY($width, $row_height);
    $pdf->Cell($behavior_width, $text_height, 'Potential:', 0, 0, 'L', false);

    $row_height += $text_height;
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFont('freesans', '', 11);
    $pdf->SetXY($width, $row_height);
    $val = '';
    if ($form->promotability_hp2) {
        $val = get_string('promotability_hp2', 'local_talentreview');
    } else if ($form->promotability_hp1) {
        $val = get_string('promotability_hp1', 'local_talentreview');
    } else if ($form->promotability_trusted) {
        $val = get_string('promotability_trusted', 'local_talentreview');
    } else if ($form->promotability_placement) {
        $val = get_string('promotability_placement', 'local_talentreview');
    } else if ($form->promotability_too_new) {
        $val = get_string('promotability_too_new', 'local_talentreview');
    }
    $pdf->Cell($behavior_width, $text_height, $val, 0, 0, 'L', false);

    /* --------------------- */
    $row_height += $text_height;
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetXY($width, $row_height);
    $pdf->Cell($behavior_width, $text_height, 'Mobility:', 0, 0, 'L', false);

    $row_height += $text_height;
    $pdf->SetXY($width, $row_height);
    $pdf->SetFont('freesans', '', 11);
    $pdf->Cell($behavior_width, $text_height, ($form->relocatability) ? 'Yes' : 'No', 0, 0, 'L', false);

    /* --------------------- */
    $row_height += $text_height+5;
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFont('freesans', 'b', 9);
    $pdf->SetXY($width, $row_height);
    $pdf->Cell($behavior_width, $text_height, 'Next Possible Role:', 0, 0, 'L', false);

    $row_height += $text_height;
    $pdf->SetXY($width, $row_height);
    $pdf->SetFont('freesans', '', 9);
    $conf = form_conf('next_possible_move');
    $pdf->MultiCell($behavior_width, $text_height, substr($form->next_possible_move, 0, $conf['num_chars']), 0, 'L');

    // ***************************************************************

    /* column 3 */
    $width += $behaviorsbox_width;
    $row_height = $height;
    $notes_width = 310;

    $pdf->SetXY($width, $row_height);
    $pdf->SetTextColor(255, 255, 255);
    $pdf->SetFont('freesans', 'b', 11);
    $pdf->SetFillColor(0, 114, 206);
    $pdf->Cell($notes_width, 20, get_string('top_behavioral_strengths', 'local_talentreview'), 1, 0, 'L', true);

    $row_height += $text_height;
    $pdf->SetXY($width, $row_height);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFont('freesans', '', 9);
    $conf = form_conf('strengths');
    $pdf->MultiCell($notes_width, 130, substr($form->strengths, 0, $conf['num_chars']), 1, 'L');
    $pdf->Ln();

    $row_height += 130;
    $pdf->SetXY($width, $row_height);
    $pdf->SetTextColor(255, 255, 255);
    $pdf->SetFont('freesans', 'b', 11);
    $pdf->SetFillColor(0, 114, 206);
    $pdf->Cell($notes_width, 20, 'Top 2-3 Behavioral Development Needs', 1, 0, 'L', true);

    $row_height += $text_height;
    $pdf->SetXY($width, $row_height);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFont('freesans', '', 9);
    $conf = form_conf('development_needs');
    $pdf->MultiCell($notes_width, 130, substr($form->development_needs, 0, $conf['num_chars']), 1, 'L');
    $pdf->Ln();

    $row_height += 130;
    $pdf->SetXY($width, $row_height);
    $pdf->SetTextColor(255, 255, 255);
    $pdf->SetFont('freesans', 'b', 11);
    $pdf->SetFillColor(0, 114, 206);
    $pdf->Cell($notes_width, 20, 'Development Plan / Overall Comments', 1, 0, 'L', true);

    $row_height += $text_height;
    $pdf->SetXY($width, $row_height);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFont('freesans', '', 9);
    $conf = form_conf('development_plan');
    $pdf->MultiCell($notes_width, 100, substr($form->development_plan, 0, $conf['num_chars']), 1, 'L', false, 1, '', '', true, 0, false, true, 80);
    $pdf->Ln();

    // ***************************************************************
    // row 6 footer
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFont('freesans', '', 10);
    $block_height = 80;

    $width = $padding;
    $height = $row_height + 80;
    $width += 120;

    $pdf->StartTransform();
    $pdf->Translate(0, 50);
    $pdf->Image($CFG->dirroot . '/local/talentreview/assets/img/service_master.jpg', $padding, $height, 150, 20);
    $pdf->StopTransform();

    $pdf->SetXY($width+20, $height);
    $pdf->Translate(0, 40);
    $pdf->SetFont('freesans', '', 1);
    $pdf->SetFillColor(50, 50, 50);
    $pdf->Cell(230, 1, '', false, 0, 'L', true);

    $pdf->SetXY($width-80, $height-10);
    $pdf->SetFont('freesans', '', 11);
    $pdf->Cell(230, 10, 'Manager Signature:', false, 0, 'L', false);

    $pdf->SetXY($width+18, $height-15);
    $pdf->SetFont('freesans', '', 12);
    $pdf->Cell(230, 10, $form->manager_signature, false, 0, 'C', false);

    $pdf->SetXY($width+18, $height+2);
    $pdf->SetFont('freesans', '', 10);
    $pdf->Cell(230, 10, (($form->manager_signature_date) ? 'Date:' .date('m/d/Y H:i, A', $form->manager_signature_date) : ''), false, 0, 'C', false);

    $width += 230;

    $pdf->StartTransform();
    $pdf->SetXY($width+160, $height);
    $pdf->SetFont('freesans', '', 1);
    $pdf->SetFillColor(50, 50, 50);
    $pdf->Cell(230, 1, '', false, 0, 'L', true);

    $pdf->SetXY($width + 50, $height - 10);
    $pdf->SetFont('freesans', '', 11);
    $pdf->Cell(230, 10, 'Employee Signature:', false, 0, 'L', false);

    $pdf->SetXY($width + 160, $height-15);
    $pdf->SetFont('freesans', '', 12);
    $pdf->Cell(230, 10, $form->signature, false, 0, 'C', false);

    $pdf->SetXY($width + 160, $height+2);
    $pdf->SetFont('freesans', '', 10);
    $pdf->Cell(230, 10, (($form->signature_date) ? 'Date:' .date('m/d/Y H:i, A', $form->signature_date) : ''), false, 0, 'C', false);

    $pdf->SetXY($width, $height+15);
    $pdf->SetFont('freesans', '', 11);
    $pdf->Cell(230, 10, 'Confidential', false, 0, 'L', false);

    $pdf->StopTransform();

    return $pdf;
}

function local_talentreview_print($form_id) {
    global $CFG, $DB, $USER;

    $form = $DB->get_record('local_talentreview', ['id' => $form_id]);
    if(!$form->id) {
        return false;
    }

    $fs = get_file_storage();
    $profile_img = '';
    if($profile_img_file = $fs->get_file_by_id($form->photo)) {
        require_once("$CFG->libdir/filelib.php");

        if (isset($CFG->filedir)) {
            $filedir = $CFG->filedir;
        } else {
            $filedir = $CFG->dataroot.'/filedir';
        }

        $profile_img_file->sync_external_file();

        // Detect is local file or not.
        $contenthash = $profile_img_file->get_contenthash();
        $l1 = $contenthash[0].$contenthash[1];
        $l2 = $contenthash[2].$contenthash[3];
        $profile_img = "{$filedir}/$l1/$l2/$contenthash";
    }

    require_once($CFG->libdir . '/pdflib.php');
    //TCPDF_FONTS::addTTFfont('/full_path_to/ARIALUNI.TTF', 'TrueTypeUnicode');

    // **************************************************************
    // generate PDF

    $pdf = new pdf('L', 'pt');
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    // generate page
    local_talentreview_print_page($form, $pdf, $profile_img);

    // Render PDF
    $pdf->Output('talentreview.pdf');

    die();
}

function cut_string($string, $lines = 4, $num_chars = 300, $chars_per_line = 120) {

    // cut lines
    $enter_count = substr_count($string, "\n");
    if($enter_count >= $lines) {
        $preg = "/";
        for($i = 0; $i < $lines; $i++) {
            $preg .= ".*\\n";
        }
        $preg .= "/";
        preg_match($preg, $string, $matches);
        if($matches && isset($matches[0])) {
            $string = $matches[0];
        }
    }

    // cut max chars
    if(strlen($string) > $num_chars) {
        $string = substr($string, 0, $num_chars) . '...';
    }

    $row = 0;
    $response = '';
    $row_num_chars = $num_chars;
    foreach (preg_split("/\\n/", $string) as $str) {
        if(!$str) {
            continue;
        }

        if(!$row_num_chars) {
            continue;
        }

        $row++;
        if(strlen($str) < $chars_per_line) {
            if(($row_num_chars - strlen($str)) >= 0) {
                $response .= $str . "\n";
            } else {
                $response .= $str . substr($str, 0, strlen($str) - abs($row_num_chars - strlen($str))) . '...';
                break;
            }
            $row_num_chars -= $chars_per_line;
        } else {
            if(($row_num_chars - (strlen($str) + $num_chars*strlen($str)%$chars_per_line)) >= 0) {
                $response .= $str . "\n";
                $row_num_chars -= strlen($str) + $num_chars*strlen($str)%$chars_per_line;
            } else {
                $response .= substr($str, 0, strlen($str) - abs($row_num_chars - strlen($str))) . '...';
                break;
            }
        }
    }
    return $response;
}

function form_conf($name) {
    $param = array(
        'goals_comment' => array(
            'lines' => 5,
            'num_chars' => 385,
            'chars_per_line' => 100
        ),
        'strengths' => array(
            'lines' => 11,
            'num_chars' => 660,
            'chars_per_line' => 50
        ),
        'development_needs' => array(
            'lines' => 11,
            'num_chars' => 660,
            'chars_per_line' => 50
        ),
        'relocatability_comments' => array(
            'lines' => 4,
            'num_chars' => 60,
            'chars_per_line' => 30
        ),
        'next_possible_move' => array(
            'lines' => 3,
            'num_chars' => 80,
            'chars_per_line' => 80
        ),
        'development_plan' => array(
            'lines' => 11,
            'num_chars' => 660,
            'chars_per_line' => 50
        ),
    );

    return isset($param[$name]) ? $param[$name] : array(
        'lines' => 4,
        'num_chars' => 80,
        'chars_per_line' => 30
    );
}

function local_talentreview_get_users($usersids, &$users, &$ids) {
    global $DB, $USER;

    $staff = local_talentreview_get_staff($usersids);

    if (count($staff)) {
        $usersids = array();
        foreach ($staff as $user) {
            $users[$user->id] = $user;
            if (!isset($ids[$user->id])){
                $usersids[$user->id] = $user->id;
            }
            $ids[$user->id] = $user->id;
        }

        if (count($usersids)){
            local_talentreview_get_users($usersids, $users, $ids);
        }
    }
}

/**
 * Returns the staff of the specified user
 *
 * @param int $userid ID of a user to get the staff of, If $userid is not set, returns staff of current user
 * @param mixed $postype Type of the position to check (POSITION_TYPE_* constant). Defaults to primary position (optional)
 * @param bool $sort optional ordering by lastname, firstname
 * @return array Array of userids of staff who are managed by user $userid , or false if none
 **/
function local_talentreview_get_staff($userids = array()) {
    global $CFG, $DB, $USER;

    require_once($CFG->dirroot.'/totara/hierarchy/prefix/position/lib.php');

    $postype = POSITION_TYPE_PRIMARY;
    $now = time();

    $params = array('type' => $postype);
    $usernamefields = get_all_user_name_fields(true, 'u');

    $orderby = "ORDER BY u.lastname ASC, u.firstname ASC";
    $select = "u.id, u.suspended, {$usernamefields}";

    if (!empty($CFG->enabletempmanagers) && $postype == POSITION_TYPE_PRIMARY) {
        // Include temporary staff.
        list($sql_in1, $params1) = $DB->get_in_or_equal($userids, SQL_PARAMS_NAMED);
        list($sql_in2, $params2) = $DB->get_in_or_equal($userids, SQL_PARAMS_NAMED);

        $sql = "SELECT DISTINCT $select
                  FROM {user} u
             LEFT JOIN {pos_assignment} pa ON (pa.userid = u.id AND pa.type = :type AND pa.managerid $sql_in1)
             LEFT JOIN {temporary_manager} tm ON (tm.userid = u.id AND tm.tempmanagerid $sql_in2 AND tm.expirytime > :expiry)
                 WHERE u.deleted = 0 AND (pa.id IS NOT NULL OR tm.id IS NOT NULL)
              $orderby";

        $params += $params1 + $params2 + array('expiry' => $now);
        $staff = $DB->get_records_sql($sql, $params);

    } else {
        // This works because:
        // - old pos_assignment records are deleted when a user is deleted by {@link delete_user()}
        //   so no need to check if the record is for a real user
        // - there is a unique key on (type, userid) on pos_assignment so no need to use
        //   DISTINCT on the userid
        list($sql_in, $params1) = $DB->get_in_or_equal($userids, SQL_PARAMS_NAMED);

        $sql = "SELECT $select
                  FROM {user} u
                  JOIN {pos_assignment} pa ON (pa.userid = u.id AND pa.type = :type AND pa.managerid $sql_in)
                 WHERE u.deleted = 0
              $orderby";

        $params += $params1;
        $staff = $DB->get_records_sql($sql, $params);
    }

    return $staff;
}


function local_talentreview_calculate_timeinrole($startdate = 0) {
    if (!$startdate) return '';

    $startdate = date('Y-m-d', $startdate);
    $enddate = date('Y-m-d');

    $diff = abs(strtotime($enddate) - strtotime($startdate));
    $years = floor($diff / (365*60*60*24));
    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

    $date = '';
    $date .= ($years) ? $years.' years ' : '';
    $date .= ($months) ? $months.' month ' : '';
    $date .= ($days) ? $days.' days ' : '';

    return $date;
}
