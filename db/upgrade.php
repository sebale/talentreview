<?php

function xmldb_local_talentreview_upgrade($oldversion) {
    global $DB;
    $dbman = $DB->get_manager();

    if ($oldversion < 2016110402) {
        if ($dbman->table_exists('local_talentreview')) {

            $table = new xmldb_table('local_talentreview');

            if(!$dbman->field_exists('local_talentreview', 'education')) {
                $dbman->add_field($table, new xmldb_field('education', XMLDB_TYPE_CHAR, 64, null, null, null, ''));
            }

            if(!$dbman->field_exists('local_talentreview', 'previous_employer')) {
                $dbman->add_field($table, new xmldb_field('previous_employer', XMLDB_TYPE_CHAR, 64, null, null, null, ''));
            }
        }
    }

    if ($oldversion < 2016110704) {
        if (!$dbman->table_exists('local_talentreview_managers')) {

            $table = new xmldb_table('local_talentreview_managers');
            $table->add_field('id', XMLDB_TYPE_INTEGER, 11, null, null, true);
            $table->add_field('role_id', XMLDB_TYPE_INTEGER, 10, null, null, null, 0);
            $table->add_field('status', XMLDB_TYPE_INTEGER, 10, null, null, null, 0);
            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2017071601) {
        if ($dbman->table_exists('local_talentreview')) {

            $table = new xmldb_table('local_talentreview');

            if(!$dbman->field_exists('local_talentreview', 'has_save')) {
                $dbman->add_field($table, new xmldb_field('has_save', XMLDB_TYPE_INTEGER, 10, null, null, null, 0));
            }
        }
    }

    if ($oldversion < 2017101100) {
        if ($dbman->table_exists('local_talentreview')) {

            $table = new xmldb_table('local_talentreview');

            if(!$dbman->field_exists('local_talentreview', 'timeinrole')) {
                $dbman->add_field($table, new xmldb_field('timeinrole', XMLDB_TYPE_CHAR, 64, null, null, null, ''));
            }

            if(!$dbman->field_exists('local_talentreview', 'svmhiredate')) {
                $dbman->add_field($table, new xmldb_field('svmhiredate', XMLDB_TYPE_INTEGER, 10, null, null, null, 0));
            }

            if(!$dbman->field_exists('local_talentreview', 'degree')) {
                $dbman->add_field($table, new xmldb_field('degree', XMLDB_TYPE_CHAR, 64, null, null, null, ''));
            }

            if(!$dbman->field_exists('local_talentreview', 'university_school')) {
                $dbman->add_field($table, new xmldb_field('university_school', XMLDB_TYPE_CHAR, 128, null, null, null, ''));
            }

            if(!$dbman->field_exists('local_talentreview', 'overal_review_rating_previous')) {
                $dbman->add_field($table, new xmldb_field('overal_review_rating_previous', XMLDB_TYPE_INTEGER, 1, null, null, null, 0));
            }

            if(!$dbman->field_exists('local_talentreview', 'goals_rating')) {
                $dbman->add_field($table, new xmldb_field('goals_rating', XMLDB_TYPE_CHAR, 64, null, null, null, ''));
            }
            if(!$dbman->field_exists('local_talentreview', 'goal2_rating')) {
                $dbman->add_field($table, new xmldb_field('goal2_rating', XMLDB_TYPE_CHAR, 64, null, null, null, ''));
            }
            if(!$dbman->field_exists('local_talentreview', 'goal3_rating')) {
                $dbman->add_field($table, new xmldb_field('goal3_rating', XMLDB_TYPE_CHAR, 64, null, null, null, ''));
            }
            if(!$dbman->field_exists('local_talentreview', 'goal4_rating')) {
                $dbman->add_field($table, new xmldb_field('goal4_rating', XMLDB_TYPE_CHAR, 64, null, null, null, ''));
            }

            if(!$dbman->field_exists('local_talentreview', 'goal2_comment')) {
                $dbman->add_field($table, new xmldb_field('goal2_comment', XMLDB_TYPE_TEXT, null, null, null, null, ''));
            }
            if(!$dbman->field_exists('local_talentreview', 'goal3_comment')) {
                $dbman->add_field($table, new xmldb_field('goal3_comment', XMLDB_TYPE_TEXT, null, null, null, null, ''));
            }
            if(!$dbman->field_exists('local_talentreview', 'goal4_comment')) {
                $dbman->add_field($table, new xmldb_field('goal4_comment', XMLDB_TYPE_TEXT, null, null, null, null, ''));
            }
        }
    }

    if ($oldversion < 2017101102) {
        if ($dbman->table_exists('local_talentreview')) {

            $table = new xmldb_table('local_talentreview');

            if(!$dbman->field_exists('local_talentreview', 'signature')) {
                $dbman->add_field($table, new xmldb_field('signature', XMLDB_TYPE_CHAR, 128, null, null, null, ''));
            }

            if(!$dbman->field_exists('local_talentreview', 'signature_date')) {
                $dbman->add_field($table, new xmldb_field('signature_date', XMLDB_TYPE_INTEGER, 10, null, null, null, 0));
            }

            if(!$dbman->field_exists('local_talentreview', 'manager_signature')) {
                $dbman->add_field($table, new xmldb_field('manager_signature', XMLDB_TYPE_CHAR, 128, null, null, null, ''));
            }

            if(!$dbman->field_exists('local_talentreview', 'manager_signature_date')) {
                $dbman->add_field($table, new xmldb_field('manager_signature_date', XMLDB_TYPE_INTEGER, 10, null, null, null, 0));
            }
        }
    }


    return true;
}
