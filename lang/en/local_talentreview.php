<?php

$string['pluginname'] = 'Talent Review Assessment';
$string['form'] = 'Form';
$string['employess'] = 'Employees';
$string['managers'] = 'Managers';
$string['create_form'] = 'Create Form';
$string['update_form'] = 'Update Form';
$string['actions'] = 'Actions';
$string['company_date'] = 'Company Date';
$string['download_pdf'] = 'Download PDF';
$string['copy_form'] = 'Copy User Form';

$string['image'] = 'Photo';
$string['name'] = 'Name';
$string['title'] = 'Job Title';
$string['manager'] = 'Manager';
$string['user_name'] = 'User';
$string['assign_employess'] = 'Assign Employess';
$string['assigned'] = 'Assigned';
$string['select_users'] = 'Users';
$string['login'] = 'Username';
$string['email'] = 'E-Mail';
$string['lastlogin'] = 'Last Login';

$string['bu_function'] = 'BU/Function';
$string['position_date'] = 'Position Date';
$string['cobehaviors_overalmpany_date'] = 'Company Date';
$string['jde_eeid'] = 'JDE/EEID #';
$string['complited_date'] = 'Completed Date';
$string['review_date'] = 'Review Date';
$string['review_year'] = 'Review Year';

$string['goals_perfomance_overal'] = 'Performance Overall (Objectives)';
$string['goals_comment'] = 'Goals Comments';

$string['perfomance_n'] = 'N: Needs Development';
$string['perfomance_a'] = 'A: At Standard';
$string['perfomance_e'] = 'E: Exceeds';

$string['behaviors_growth'] = 'Growth & Customer Focus';
$string['behaviors_accountability'] = 'Accountability';
$string['behaviors_self_aware'] = 'Self Aware/Learner';
$string['behaviors_champions'] = 'Champions Change';
$string['behaviors_initiative'] = 'Initiative';
$string['behaviors_judgment'] = 'Judgment';
$string['behaviors_makes_people'] = 'Makes People Better';
$string['behaviors_leadership'] = 'Leadership Impact';
$string['behaviors_effective_com'] = 'Effective Communicator';
$string['behaviors_gets_result'] = 'Gets Results';
$string['behaviors_integrative'] = 'Integrative Thinker';
$string['behaviors_intelligent'] = 'Intelligent Risk Taker';
$string['behaviors_overal'] = 'Behaviors Overall';

$string['strengths'] = 'Strengths';
$string['development_needs'] = 'Development Needs';

$string['promotability'] = 'Potential';
$string['development_plan'] = 'Development Plan/Overall Comments';

$string['promotability_hp1'] = 'Promotable Level 1';
$string['promotability_hp2'] = 'Promotable Level 2';
$string['promotability_trusted'] = 'Trusted Professional';
$string['promotability_placement'] = 'Placement Issue';
$string['promotability_too_new'] = 'Too New';

$string['comments'] = 'Comments';
$string['relocatability_group_label'] = 'Wiling to relocate?';
$string['relocatability'] = 'Relocatability';
$string['next_possible_move'] = 'Next Possible Move';

$string['box_placement'] = '9-Box Placement';
$string['approve'] = 'Approve';
$string['form_title'] = 'Job Title';
$string['menu_talentreview'] = 'Talent Review Assessment';

$string['education'] = 'Education';
$string['previous_employer'] = 'Previous Employer';

$string['employess'] = 'employess';
$string['form'] = 'form';
$string['index'] = 'index';
$string['managers'] = 'managers';

$string['preferences'] = 'Preferences';
$string['submitdate'] = 'Submission Date';
$string['submitdate_help'] = 'Date when submitted forms will be available for employees';

$string['talentreview:employess'] = 'Manage employees';
$string['talentreview:managers'] = 'Manage managers';
$string['talentreview:form'] = 'Create form';
$string['talentreview:index'] = 'View Talent Review Assessment';
$string['notset'] = 'not set';
$string['showall'] = 'Show All';
$string['active'] = 'Active';
$string['inactive'] = 'Inactive';
$string['status'] = 'Status';
$string['deletemessage'] = 'Are you sure you want to delete this form?';
$string['lockdate'] = 'Lock Date';
$string['lockdate_help'] = 'All forms will be locked after this date for Managers and Employees';
$string['hideuntildate'] = 'Hide Forms Date';
$string['hideuntildate_help'] = 'Hide Forms for employees until this date';
$string['timeinrole'] = 'Time in Role';
$string['svmhiredate'] = 'SVM Hire Date';
$string['degree'] = 'Degree';
$string['university_school'] = 'University/School';
$string['previous_work'] = 'Previous Work';
$string['goal1_rating'] = 'Goal 1 Rating';
$string['goal2_rating'] = 'Goal 2 Rating';
$string['goal3_rating'] = 'Goal 3 Rating';
$string['goal4_rating'] = 'Goal 4 Rating';
$string['goal1_comment'] = 'Goal 1 Comment';
$string['goal2_comment'] = 'Goal 2 Comment';
$string['goal3_comment'] = 'Goal 3 Comment';
$string['goal4_comment'] = 'Goal 4 Comment';
$string['behaviorrequiren'] = 'At least two (2) behaviors need to be an "N" rating selected.';
$string['submitconfirm'] = 'This action will allow the employee to view this review. Are you sure you want to continue?';
$string['signature'] = 'Signature';
$string['signature_date'] = 'Signature Date';
$string['manager_signature'] = 'Manager Signature';
$string['manager_signature_date'] = 'Manager Signature Date';
$string['signform'] = 'Sign Form';
$string['somehighschool'] = 'Some High School';
$string['highschoolged'] = 'High School/GED';
$string['somecollege'] = 'Some College';
$string['associates'] = 'Associates';
$string['bachelors'] = 'Bachelors';
$string['masters'] = 'Masters';
$string['doctorate'] = 'Doctorate';
$string['overallreviewrating'] = 'Overall Review Rating';
$string['goals_desc'] = 'Before providing comments, review the employee\'s annual goals. Then, provide the rationale behind the rating, including a brief summary of the employee’s goals and what was achieved for each goal.';
$string['behaviors_desc'] = 'Rate each behavior based on the extent to which each was demonstrated throughout the year. You are required to rate at least two (2) behaviors as N: Needs Development.';
$string['strengths'] = 'Strengths';
$string['strengths_desc'] = 'Using the ratings above, identify two-three (2-3) behaviors that are strengths. Provide a brief description of how the strengths are currently demonstrated and how they can be leveraged in the future. These behaviors are rated as E: Exceeds or A: At Standard.';
$string['developmentneeds'] = 'Development Needs';
$string['developmentneeds_desc'] = 'Using the ratings above, identify two-three (2-3) behaviors that needs development. Provide a brief description of how the development needs are currently demonstrated and how they can be improved in the future. These behaviors are rated as N: Needs Development.';
$string['promotable2_desc'] = 'Ability to successfully do work at the next management level and above; ready for increased responsibilities at current level.';
$string['promotable1_desc'] = 'Ability to successfully do work at the next management level; current role may still provide the opportunity for growth and development.';
$string['trusted_desc'] = 'Ability to successfully do work at the current level; highly valued, seasoned professional in current role.';
$string['placement_desc'] = 'Inability to successfully do work at the current level; performance/placement needs to be addressed in current role.';
$string['toonew_desc'] = '3 months or less in current position; unable to identify potential until additional time/assessment has passed.';
$string['mobility'] = 'Mobility';
$string['manager_signature_desc'] = 'Type your name to acknowledge you have delivered the review to the listed employee.';
$string['rating_1'] = '1: Below Required Performance*';
$string['rating_2'] = '2: Performance Improvement Needed*';
$string['rating_3'] = '3: Successful Performance';
$string['rating_4'] = '4: Exceptional Performance';
$string['rating_5'] = '5: Consistently Exceptional Performance';
$string['rating_req'] = '*A performance review with this rating should be accompanied by formal progressive discipline documentation such as a CAR or PIP.';
$string['top_behavioral_strengths'] = 'Top 2-3 Behavioral Strengths';

