<?php

require_once("../../config.php");
require_once("lib.php");
require_once("classes/forms/local_talentreview_notification_setting.php");

require_login();

// managers redirect to employers page
if(!is_siteadmin()) {
    redirect(new moodle_url("/local/talentreview/"));
}

$context = context_system::instance();
$title = 'Notification Settings';

$PAGE->set_context(context_system::instance());
$PAGE->set_url(new moodle_url("/local/talentreview/index.php"));
$PAGE->requires->jquery();

$PAGE->navbar->add(get_string('pluginname', 'local_talentreview'), new moodle_url('/local/talentreview/'));
$PAGE->navbar->add($title);

$PAGE->set_title($title);
$PAGE->set_pagelayout('course');
$PAGE->set_heading($title);

$PAGE->requires->css('/local/talentreview/style.css', true);

$form = new local_talentreview_notification_setting(null, []);
$form_data = $DB->get_record('local_talentreview_settings', ['id' => LocalTalentReview::$SETTINGS_ROW_ID]);
$form->set_data($form_data);

if ($form->is_cancelled()) {
    redirect(new moodle_url('/local/talentreview/managers.php'));
} else if ($data = $form->get_data()) {
    $setting = new stdClass();
    $setting->id = $form_data->id;
    $setting->notification_sendler = $data->notification_sendler;
    $setting->user_subject = $data->user_subject;
    $setting->user_message = $data->user_message;
    $setting->manager_subject = $data->manager_subject;
    $setting->manager_message = $data->manager_message;
    $setting->status = $data->status;
    $setting->updated = time();

    if($setting->id == LocalTalentReview::$SETTINGS_ROW_ID) {
        $DB->update_record('local_talentreview_settings', $setting);
    } else {
        $DB->insert_record('local_talentreview_settings', $setting);
    }
    redirect(new moodle_url('/local/talentreview/notification_settings.php'));
}

echo $OUTPUT->header();

?>

    <?php echo $OUTPUT->heading($title); ?>

    <?php echo $form->display(); ?>

<?php
echo $OUTPUT->footer();