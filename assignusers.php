<?php

require_once("../../config.php");
require_once("lib.php");
require_once("classes/forms/talentreview_assign_form.php");
require_once("classes/tables/assigned_users_table.php");

require_login();

$context = context_system::instance();
if(!is_siteadmin()) {
    throw new required_capability_exception($context, 'local/talentreview:employess', 'nopermissions', '');
}

$id = optional_param('id', 0, PARAM_INT);
$user_id = optional_param('user_id', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);

$title = 'Manage Assignments';

$PAGE->set_context(context_system::instance());
$PAGE->set_url(new moodle_url("/local/talentreview/assignusers.php"));
$PAGE->requires->jquery();

$PAGE->navbar->add(get_string('pluginname', 'local_talentreview'), new moodle_url('/local/talentreview/'));
$PAGE->navbar->add('Managers', new moodle_url('/local/talentreview/managers.php'));
$PAGE->navbar->add($title);

$PAGE->set_title($title);
$PAGE->set_pagelayout('course');
$PAGE->set_heading($title);

$PAGE->requires->css('/local/talentreview/style.css', true);

echo $OUTPUT->header();

$table = new assigned_users_table();
$table->show_download_buttons_at([]);
$table->is_collapsible = false;

$editform = new talentreview_assign_form(null, array('id' => $id));
if ($editform->is_cancelled()) {
    redirect(new moodle_url('/local/talentreview/managers.php'));
} else if ($data = $editform->get_data()) {
    if($data->ids) {
        foreach (explode(',', $data->ids)  as $user_id) {
            $assign = new stdClass();
            $assign->employer_id = $user_id;
            $assign->manager_id = $data->manager_id;
            $assign->created = time();
            $DB->insert_record('local_talentreview_assign', $assign);
        }
    }
    redirect(new moodle_url('/local/talentreview/assignusers.php', ['id' => $data->manager_id]));
}
?>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<?php echo $OUTPUT->heading($title); ?>

    <div class="controls controls-row">
        <div class="span8">
            <?php echo $editform->display(); ?>
        </div>
        <div class="span4">
            <?php
                $img = html_writer::tag('img', '', array(
                    'src' => $OUTPUT->pix_url('a/download_all'),
                    'height' => '15'
                ));
                echo html_writer::tag('a', $img, array(
                    'href' => $CFG->wwwroot . '/local/talentreview/import.php?id=' . $id,
                    'class' => 'btn pull-right',
                    'title' => 'Assign users from csv'
                ));
            ?>
        </div>
    </div>

<?php $table->out(20, true); ?>

    <script type="text/javascript">
        $(function () {
            $(document).ready(function() {
                $('#mform1').submit(function() {
                    $('input[name="ids"]').val($('#id_users').val());
                });

                function formatUser(data) {
                    if (data.loading) {
                        return data.name;
                    }
                    return data.name;
                }

                function formatUserSelection (item) {
                    return item.name;
                }

                $("#id_users").select2({
                    minimumInputLength: 1,
                    tags: [],
                    ajax: {
                        url: "<?php echo $CFG->wwwroot; ?>/local/talentreview/ajax.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term, // search term
                                page: params.page,
                                manager: <?php echo $id ?>
                            };
                        },
                        processResults: function (data, params) {
                            // parse the results into the format expected by Select2
                            // since we are using custom formatting functions we do not need to
                            // alter the remote JSON data, except to indicate that infinite
                            // scrolling can be used
                            params.page = params.page || 1;
                            //console.log(data.items);
                            return {
                                results: data.items,
                                pagination: {
                                    more: (params.page * 20) < data.total_count
                                }
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    },
                    templateResult: formatUser,
                    templateSelection: formatUserSelection
                });
            });
        });
    </script>
<?php
echo $OUTPUT->footer();