<?php

require_once("../../config.php");
require_once("lib.php");
require_once("locallib.php");
require_once("classes/forms/learner_html_form.php");

require_login();

$context = context_system::instance();
require_capability('local/talentreview:form', $context);

$form_id = optional_param('id', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);
$to_user = optional_param('to_user', 0, PARAM_INT);
$manager_id = optional_param('manager_id', 0, PARAM_INT);
$post_user_id = optional_param('user_id', 0, PARAM_INT);
$cancel = optional_param('cancel', '', PARAM_TEXT);

// prepare user or manager
$user_id = $to_user ? $to_user : ($post_user_id ? $post_user_id : $USER->id);
$manager_id = $manager_id ? $manager_id : 0;

$redirect_url = new moodle_url('/local/talentreview/');
if(is_siteadmin($USER->id)) {
    $redirect_url = new moodle_url('/local/talentreview/form_managment.php');
} elseif (LocalTalentReview::has_manager_access()) {
    $redirect_url = new moodle_url('/local/talentreview/employees.php');
}

switch ($action) {
    case 'copy':
    case 'approve':
    case 'block':
    case 'delete':
        // use go down
        break;

    case 'print':
        // generate PDF
        local_talentreview_print($form_id);
        break;
}

$title = 'Talent Review Form';

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('course');
$PAGE->set_heading($title);
$PAGE->set_title($title);
$PAGE->set_url(new moodle_url('/local/talentreview/form.php'));

$PAGE->navbar->add(get_string('pluginname', 'local_talentreview'), new moodle_url('/local/talentreview/'));
$PAGE->navbar->add($title);

$PAGE->requires->jquery();
$PAGE->requires->css('/local/talentreview/assets/lib/jquery-ui/jquery-ui.min.css', true);
$PAGE->requires->css('/local/talentreview/style.css', true);

$PAGE->requires->js('/local/talentreview/assets/lib/jquery-ui/jquery-ui.min.js', true);

$form_data = new stdClass();

if($form_id) {
    $form_data = $DB->get_record('local_talentreview', ['id' => $form_id]);

    if(!LocalTalentReview::has_manager_access() && $form_data->user_id != $USER->id) {
        throw new required_capability_exception($context, 'local/talentreview:employess', 'Sorry, but you do not currently have permissions to do that', '');
    }

    if($action == 'approve') {
        if(LocalTalentReview::has_manager_access()) {
            $form_data->review_date = time();
            $form_data->approve = true;
            $DB->update_record('local_talentreview', $form_data);
        }
        if(is_siteadmin($USER->id)) {
            redirect(new moodle_url('/local/talentreview/form_managment.php', []));
        } else {
            redirect(new moodle_url('/local/talentreview/employees.php'));
        }
    }

    if($action == 'block') {
        $form_data->approve = false;
        $DB->update_record('local_talentreview', $form_data);
        redirect(new moodle_url('/local/talentreview/employees.php'));
    }

    if($action == 'copy') {
        $form_id = 0;
        $form_data->id = null;
        $form_data->copy = 1;

        // update date created
        $form_data->complited_date = time();

        // update form owner
        $form_data->form_origin = LocalTalentReview::checkFormOrigin($USER->id, $form_data);
    }

    if($action == 'delete') {
        $DB->delete_records('local_talentreview', ['id' => $form_id]);

        if(is_siteadmin($USER->id)) {
            redirect(new moodle_url('/local/talentreview/form_managment.php', []));
        } elseif (LocalTalentReview::has_manager_access() && $form_data->user_id != $USER->id) {
            redirect($redirect_url);
        } else {
            redirect(new moodle_url('/local/talentreview/', []));
        }
    }
} else {

    $user = $DB->get_record('user', [
        'id' => $user_id
    ]);

    $form_data->photo = $user->picture;
    $form_data->user_id = $user->id;
    $form_data->name = $user->firstname . ' ' . $user->lastname;

    if(!$manager_id) {
        $assign_record = $DB->get_record_sql('SELECT id, managerid FROM {pos_assignment} WHERE managerid <> "" AND userid = ' . $user_id);
        if($assign_record) {
            $manager_id = $assign_record->managerid;
        }
    }

    if($manager_record = $DB->get_record('user', array('id' => $manager_id), 'id, firstname, lastname')) {
        $form_data->manager = $manager_record->firstname . ' ' . $manager_record->lastname;
    }

    $user_info = $DB->get_records_sql('SELECT f.id, f.shortname, d.data, d.dataformat
                                       FROM {user_info_field} as f
                                       LEFT JOIN {user_info_data} as d ON d.fieldid = f.id
                                       WHERE f.shortname IN ("education", "previousemployer", "bufunction", "positiondate", "companydate", "jdeeeid", "jobtitle", "svmhiredate", "overal_review_rating_previous")
                                        AND d.userid = ' . $user_id);

    $custom_data = [];
    foreach ($user_info as $data) {
        $custom_data[$data->shortname] = $data->data;
    }

    $form_data->bu_function = isset($custom_data['bufunction']) ? $custom_data['bufunction'] : '';
    $form_data->jde_eeid = isset($custom_data['jdeeeid']) ? $custom_data['jdeeeid'] : '';
    $form_data->title = isset($custom_data['jobtitle']) ? $custom_data['jobtitle'] : '';
    $form_data->previous_employer = isset($custom_data['previousemployer']) ? $custom_data['previousemployer'] : '';
    //$form_data->timeinrole = isset($custom_data['timeinrole']) ? $custom_data['timeinrole'] : '';
    $form_data->svmhiredate = isset($custom_data['svmhiredate']) ? $custom_data['svmhiredate'] : '';
    $form_data->degree = isset($custom_data['degree']) ? $custom_data['degree'] : '';
    $form_data->university_school = isset($custom_data['university_school']) ? $custom_data['university_school'] : '';
    $form_data->overal_review_rating_previous = isset($custom_data['overal_review_rating_previous']) ? $custom_data['overal_review_rating_previous'] : '';
    $form_data->goals_rating = isset($custom_data['goals_rating']) ? $custom_data['goals_rating'] : '';
    $form_data->goal2_rating = isset($custom_data['goal2_rating']) ? $custom_data['goal2_rating'] : '';
    $form_data->goal3_rating = isset($custom_data['goal3_rating']) ? $custom_data['goal3_rating'] : '';
    $form_data->goal4_rating = isset($custom_data['goal4_rating']) ? $custom_data['goal4_rating'] : '';
    $form_data->goal2_comment = isset($custom_data['goal2_comment']) ? $custom_data['goal2_comment'] : '';
    $form_data->goal3_comment = isset($custom_data['goal3_comment']) ? $custom_data['goal3_comment'] : '';
    $form_data->goal4_comment = isset($custom_data['goal4_comment']) ? $custom_data['goal4_comment'] : '';
    $form_data->signature = isset($custom_data['signature']) ? $custom_data['signature'] : '';
    $form_data->signature_date = isset($custom_data['signature_date']) ? $custom_data['signature_date'] : '';
    $form_data->manager_signature = isset($custom_data['manager_signature']) ? $custom_data['manager_signature'] : '';
    $form_data->manager_signature_date = isset($custom_data['manager_signature_date']) ? $custom_data['manager_signature_date'] : '';

    $form_data->complited_date = time();
    $form_data->form_origin = LocalTalentReview::checkFormOrigin($USER->id, $form_data);
}

$form = new learner_html_form(null, [
    'user_id' => ($form_data->user_id) ? $form_data->user_id : $user_id,
    'form_id' => $form_id,
    'manager_id' => $manager_id,
    'data' => $form_data
]);
$form->set_data($form_data);
if ($form->is_cancelled() or !empty($cancel)) {
    redirect($redirect_url);
} else if ($post = $form->get_data()) {
    $insert_record = new stdClass();
    $insert_record->id = $post->id;
    $insert_record->user_id = $post->user_id;
    $insert_record->jde_eeid = $post->jde_eeid;

    // upload image after create record talentreview
    $insert_record->photo = $post->photo;

    $review_date = $post->review_date;
    $insert_record->approve = $post->approve;

    if(isset($_REQUEST['submitbutton'])) {
        if (!$insert_record->approve && LocalTalentReview::hasNeedApproved($USER->id, $post->user_id)) {
            $review_date = time();
            $insert_record->approve = true;
        }
    } else if ($post->copy) {
        $review_date = time();
        $post->review_year = date('Y');
    }

    $insert_record->name = $post->name;
    $insert_record->title = $post->title;
    $insert_record->manager = $post->manager;
    $insert_record->bu_function = $post->bu_function;
    $insert_record->complited_date = $post->complited_date;
    $insert_record->review_date = $review_date;
    $insert_record->goals_perfomance_overal = $post->goals_perfomance_overal;
    $insert_record->goals_comment = $post->goals_comment;
    $insert_record->behaviors_growth = $post->behaviors_growth;
    $insert_record->behaviors_accountability = $post->behaviors_accountability;
    $insert_record->behaviors_self_aware = $post->behaviors_self_aware;
    $insert_record->behaviors_champions = $post->behaviors_champions;
    $insert_record->behaviors_initiative = $post->behaviors_initiative;
    $insert_record->behaviors_judgment = $post->behaviors_judgment;
    $insert_record->behaviors_makes_people = $post->behaviors_makes_people;
    $insert_record->behaviors_leadership = $post->behaviors_leadership;
    $insert_record->behaviors_effective_com = $post->behaviors_effective_com;
    $insert_record->behaviors_gets_result = $post->behaviors_gets_result;
    $insert_record->behaviors_integrative = $post->behaviors_integrative;
    $insert_record->behaviors_intelligent = $post->behaviors_intelligent;
    $insert_record->behaviors_overal = $post->behaviors_overal;
    $insert_record->strengths = $post->strengths;
    $insert_record->development_needs = $post->development_needs;
    $insert_record->development_plan = $post->development_plan;
    $insert_record->next_possible_move = $post->next_possible_move;
    $insert_record->relocatability = $post->relocatability;
    $insert_record->relocatability_comments = $post->relocatability_comments;
    $insert_record->promotability_hp1 = isset($post->promotability_hp1) ? $post->promotability_hp1 : false;
    $insert_record->promotability_hp2 = isset($post->promotability_hp2) ? $post->promotability_hp2 : false;
    $insert_record->promotability_trusted = isset($post->promotability_trusted) ? $post->promotability_trusted : false;
    $insert_record->promotability_placement = isset($post->promotability_placement) ? $post->promotability_placement : false;
    $insert_record->promotability_too_new = isset($post->promotability_too_new) ? $post->promotability_too_new : false;
    $insert_record->form_origin = $post->form_origin;
    $insert_record->review_year = $post->review_year;
    $insert_record->overal_review_rating = $post->overal_review_rating;
    $insert_record->previous_employer = $post->previous_employer;
    //$insert_record->timeinrole = $post->timeinrole;
    $insert_record->svmhiredate = $post->svmhiredate;
    $insert_record->degree = $post->degree;
    $insert_record->university_school = $post->university_school;
    $insert_record->overal_review_rating_previous = $post->overal_review_rating_previous;
    $insert_record->goals_rating = $post->goals_rating;
    $insert_record->goal2_rating = $post->goal2_rating;
    $insert_record->goal3_rating = $post->goal3_rating;
    $insert_record->goal4_rating = $post->goal4_rating;
    $insert_record->goal2_comment = $post->goal2_comment;
    $insert_record->goal3_comment = $post->goal3_comment;
    $insert_record->goal4_comment = $post->goal4_comment;
    $insert_record->signature = $post->signature;
    $insert_record->manager_signature = $post->manager_signature;

    if (isset($post->manager_signature) and !empty($post->manager_signature) and empty($form_data->manager_signature)) {
        $insert_record->manager_signature_date = time();
    }
    if (isset($post->signature) and !empty($post->signature) and empty($form_data->signature)) {
        $insert_record->signature_date = time();
    }

    if (isset($_REQUEST['save_button']) and $form_data->has_save == LocalTalentReview::$SAVE_STATUS_SUBMIT and $user->id == $USER->id and !$post->copy) {
        $insert_record->has_save = LocalTalentReview::$SAVE_STATUS_SUBMIT;
    } elseif(isset($_REQUEST['save_button'])) {
        if (($form_data->has_save == LocalTalentReview::$SAVE_STATUS_SUBMIT or $form_data->approve) and $user->id != $USER->id) {
            $insert_record->approve = false;
        }
        $insert_record->has_save = LocalTalentReview::$SAVE_STATUS_SAVE;
    } elseif(isset($_REQUEST['submitbutton'])) {
        if ($post->copy) {
            $insert_record->has_save = LocalTalentReview::$SAVE_STATUS_SAVE;
        } else {
            $insert_record->has_save = LocalTalentReview::$SAVE_STATUS_SUBMIT;
        }
    } else {
        $insert_record->has_save = LocalTalentReview::$SAVE_STATUS_DEFAULT;
    }

    if($form_id) {
        $DB->update_record('local_talentreview', $insert_record);
    } else {
        if ($post->copy) {
            $insert_record->approve = false;
        }
        $post->id = $DB->insert_record('local_talentreview', $insert_record);
    }
    talentreview_update_picture($post, $form);

    // **************************************************
    // notification block
    $notification_settings = $DB->get_record('local_talentreview_settings', ['id' => LocalTalentReview::$SETTINGS_ROW_ID]);
    if($notification_settings && $notification_settings->id == LocalTalentReview::$SETTINGS_ROW_ID && $notification_settings->status) {

        $user = $DB->get_record('user', [
            'id' => $insert_record->user_id
        ]);

        $assign_record = $DB->get_record('pos_assignment', ['userid' => $user_id], 'id, managerid');
        $manager = $DB->get_record('user', [
            'id' => $assign_record->managerid
        ]);

        $user_name = $user->firstname . ' ' . $user->lastname;
        $manager_name = ($manager ? ($manager->firstname . ' ' . $manager->lastname) : '');
        $not_url = "{$CFG->wwwroot}/local/talentreview/form.php?id={$post->id}&action=print";

        $notification_settings->user_subject = str_replace('%user%', $user_name, $notification_settings->user_subject);
        $notification_settings->user_subject = str_replace('%manager%', $manager_name, $notification_settings->user_subject);
        $notification_settings->user_message = str_replace('%user%', $user_name, $notification_settings->user_message);
        $notification_settings->user_message = str_replace('%manager%', $manager_name, $notification_settings->user_message);
        $notification_settings->user_message = str_replace('%url%', $not_url, $notification_settings->user_message);

        $not_url = "{$CFG->wwwroot}/local/talentreview/form.php?id={$post->id}";

        $notification_settings->manager_subject = str_replace('%user%', $user_name, $notification_settings->manager_subject);
        $notification_settings->manager_subject = str_replace('%manager%', $manager_name, $notification_settings->manager_subject);
        $notification_settings->manager_message = str_replace('%user%', $user_name, $notification_settings->manager_message);
        $notification_settings->manager_message = str_replace('%manager%', $manager_name, $notification_settings->manager_message);
        $notification_settings->manager_message = str_replace('%url%', $not_url, $notification_settings->manager_message);

        if ($user->id != $USER->id &&
            LocalTalentReview::has_manager_access() &&
            !LocalTalentReview::has_manager_access($user->id)) {
            email_to_user($user, $notification_settings->notification_sendler, $notification_settings->user_subject, $notification_settings->user_message);
        }

        if ($user->id == $USER->id &&
            !LocalTalentReview::has_manager_access() &&
            $insert_record->has_save == LocalTalentReview::$SAVE_STATUS_SUBMIT) {
            email_to_user($manager, $notification_settings->notification_sendler, $notification_settings->manager_subject, $notification_settings->manager_message);
        }
    }

    if(is_siteadmin($USER->id)) {
        redirect(new moodle_url('/local/talentreview/form_managment.php', []));
    } elseif(LocalTalentReview::has_manager_access() && $insert_record->user_id != $USER->id) {
        redirect($redirect_url);
    } else {
        redirect(new moodle_url('/local/talentreview/', []));
    }
    exit();
}
?>

<?php
/*$conf = form_conf('goals_comment');
$verify_goals_comment = cut_string($form_data->goals_comment, $conf['lines'], $conf['num_chars'], $conf['chars_per_line']);

echo '<pre>';
print_r(form_conf('goals_comment'));
echo '|' . trim($form_data->goals_comment) . '|' . '<br>';
echo '|' . trim($verify_goals_comment) . '|' . '<br>';
echo '</pre>';
exit();
*/?>

<?= $OUTPUT->header(); ?>

<?php echo $OUTPUT->heading(get_string(($form_id ? 'update_form' : 'create_form'), 'local_talentreview')); ?>

<?php
    $form->display();
?>
    <script type="text/javascript">
        $(function() {
            $('label[for="id_behaviors_growth"]').attr('title', 'Recognizes the need to think and act differently in order to grow. Listening to our customers and delivering superb service every day builds trust and opens the door to new sales.')
            $('label[for="id_behaviors_accountability"]').attr('title', 'Is the willingness to accept responsibility for your actions, and take ownership for results, regardless of the outcome.');
            $('label[for="id_behaviors_self_aware"]').attr('title', 'Recognizes how behaviors affect others. They’re in tune with their own strengths and weaknesses and take actions to improve. They readily accept feedback, learn from mistakes and demonstrate commitment to change their behaviors.');
            $('label[for="id_behaviors_champions"]').attr('title', 'Means adopting a continuous improvement mindset that reflects a commitment to do things better… and enlisting others to challenge the status quo and drive transformation.');
            $('label[for="id_behaviors_initiative"]').attr('title', 'Is the energy and desire to do something before others do it. – without waiting or needing to be told. Seeing what needs to be done and doing it.');
            $('label[for="id_behaviors_judgment"]').attr('title', 'Is the ability to make good decisions – sometimes with limited information – about what needs to get done. Good judgment requires confidence/perspective – and trust that doing the right thing is always the right thing to do.');
            $('label[for="id_behaviors_makes_people"]').attr('title', 'Requires continuous learning and professional development through timely, constructive and candid feedback. .. then providing the tools, resources and training that help people excel.');
            $('label[for="id_behaviors_leadership"]').attr('title', 'Means thinking like a leader, regardless of your job, and creating or embracing a vision that motivates others. With actions consistently matching your words, you’ll be seen as trusted, truthful and credible.');
            $('label[for="id_behaviors_effective_com"]').attr('title', 'Means providing timely, candid, two-way communication, using clear and thoughtful oral and written communications. They spend at least as much time listening as they do talking – and constantly work toward building trust.');
            $('label[for="id_behaviors_gets_result"]').attr('title', 'Requires meeting commitments to customers, investors and each other. Acting with a sense of urgency, leaders quickly translate requirements into actions – and execute.');
            $('label[for="id_behaviors_integrative"]').attr('title', 'Recommend and take action by using a combination of experience, intuition and judgment. Thinking strategically to address the critical issues in even the most complex situation.');
            $('label[for="id_behaviors_intelligent"]').attr('title', 'Recognizes that generating greater returns often requires taking greater risks. Using sound business judgment, these individuals have the courage to take action when outcomes are uncertain, but the potential rewards are great.');
            /*$('label[for="id_behaviors_overal"]').attr('title', '');*/

            // use tooltip
            $('form').tooltip();


            $('#id_promotability_hp1, #id_promotability_hp2, #id_promotability_trusted, #id_promotability_placement, #id_promotability_too_new').on('change', function () {

                /*if ($(this).prop('checked')) {
                    console.log($(this).val());
                }*/

                if($('#id_promotability_hp1').attr('name') != $(this).attr('name')) {
                    $('#id_promotability_hp1').prop('checked', false);
                }

                if($('#id_promotability_hp2').attr('name') != $(this).attr('name')) {
                    $('#id_promotability_hp2').prop('checked', false);
                }
                if($('#id_promotability_trusted').attr('name') != $(this).attr('name')) {
                    $('#id_promotability_trusted').prop('checked', false);
                }
                if($('#id_promotability_placement').attr('name') != $(this).attr('name')) {
                    $('#id_promotability_placement').prop('checked', false);
                }
                if($('#id_promotability_too_new').attr('name') != $(this).attr('name')) {
                    $('#id_promotability_too_new').prop('checked', false);
                }
            });
        });
    </script>

<?php

echo $OUTPUT->footer();
