<?php


require_once("../../config.php");
require_once("lib.php");
require_once("classes/tables/managers_table.php");
require_once("classes/forms/talentreview_search_user_form.php");

$search = optional_param('search', '', PARAM_RAW);
$statusfilter = optional_param('statusfilter', 2, PARAM_INT);

require_login();

$context = context_system::instance();
if ($statusfilter < 2) {
    set_user_preference('statusfilter', $statusfilter);
}

// page use only admin
if(!is_siteadmin()) {
    throw new required_capability_exception($context, 'local/talentreview:managers', 'Sorry, but you do not currently have permissions to do that', '');
}

$title = 'Managers';

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('course');
$PAGE->set_heading($title);
$PAGE->set_title($title);
$PAGE->set_url(new moodle_url('/local/talentreview/managers.php'));

$PAGE->navbar->add(get_string('pluginname', 'local_talentreview'), new moodle_url('/local/talentreview/'));
$PAGE->navbar->add($title);

$PAGE->requires->jquery();
$PAGE->requires->css('/local/talentreview/style.css', true);

$table = new managers_table('managers_table', $search);
$table->show_download_buttons_at([]);
$table->is_collapsible = false;

$editform = new talentreview_search_user_form();
$editform->set_data(array('search' => $search));
if ($editform->is_cancelled()) {
    redirect(new moodle_url('/local/talentreview/managers.php'));
} else if ($data = $editform->get_data()) {
    redirect(new moodle_url('/local/talentreview/managers.php', array('search' => $data->search)));
}


echo $OUTPUT->header();
?>
    <div class="controls controls-row">
        <div class="span8">
            <?php echo $OUTPUT->heading($title); ?>
        </div>
    </div>
    <div class="controls controls-row">
        <div class="span12 clearfix">
            <div class="btn-group pull-right">
                <button class="btn dropdown-toggle" data-toggle="dropdown">
                    Settings
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo $CFG->wwwroot . '/local/talentreview/notification_settings.php'; ?>">Notification</a></li>
                    <li><a href="<?php echo $CFG->wwwroot . '/local/talentreview/form_managment.php'; ?>">Form Managment</a></li>
                    <li><a href="<?php echo $CFG->wwwroot . '/local/talentreview/preferences.php'; ?>"><?php echo get_string('preferences', 'local_talentreview'); ?></a></li>
                </ul>
            </div>
            <div class="btn-group pull-right">
                <?php echo $editform->display(); ?>
            </div>
        </div>
    </div>

<?php $table->out(20, true); ?>

<?php
echo $OUTPUT->footer();
