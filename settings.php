<?php

defined('MOODLE_INTERNAL') || die;

global $CFG;

require_once($CFG->dirroot . "/local/talentreview/lib.php");

$settings = new admin_settingpage('local_talentreview', get_string('pluginname', 'local_talentreview'));

if (!$ADMIN->locate('talentreview') && $ADMIN->locate('localplugins')) {
    $ADMIN->add('localplugins', new admin_category('talentreview', get_string('pluginname', 'local_talentreview')));
    $ADMIN->add('localplugins', $settings);
}

if(LocalTalentReview::has_manager_access() && !is_siteadmin()) {
    $ADMIN->add('root', new admin_externalpage('talentreview_admin', get_string('pluginname', 'local_talentreview'), $CFG->wwwroot . '/local/talentreview/', 'local/talentreview:managers'));
}

if(is_siteadmin()) {
    $ADMIN->add('root', new admin_externalpage('talentreview_admin', get_string('pluginname', 'local_talentreview'), $CFG->wwwroot . '/local/talentreview/managers.php', 'local/talentreview:managers'));
}

if(is_siteadmin()) {

}

?>
