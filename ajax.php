<?php

require_once("../../config.php");
require_once("lib.php");

require_login();

$context = context_system::instance();
if(!is_siteadmin()) {
    throw new required_capability_exception($context, 'local/talentreview:employess', 'nopermissions', '');
}

$search_query = optional_param('q', '', PARAM_TEXT);
$page = optional_param('page', 0, PARAM_INT);
$limit_p = optional_param('limit', 20, PARAM_INT);
$manager_id = optional_param('manager', 0, PARAM_INT);

$my_assign = $DB->get_record('local_talentreview_assign', array('employer_id' => $manager_id));

$search_field = 'concat(u.firstname, " ", u.lastname, " (", u.username, ")")';
$select = 'SELECT u.id, ' . $search_field . ' as title FROM {user} as u ';
$where = 'WHERE ' . $search_field . ' LIKE "%' . $search_query . '%" AND u.confirmed = 1 AND u.deleted = 0 AND u.id <> ' . $CFG->siteguest . ' ';
$where .= ' AND u.id NOT IN (SELECT employer_id FROM {local_talentreview_assign} WHERE manager_id <> ' . $manager_id . ') ';
$limit = 'LIMIT ' . (!$page ? 0 : ($page-1)*$limit_p) . ', ' . $limit_p;
$order = 'ORDER BY title ';

$records = $DB->get_records_sql($select . $where . $order . $limit);
$list = [];
foreach ($records as $item) {
    if (is_siteadmin($item->id) || ($item->id == $manager_id) || ($my_assign && $item->id == $my_assign->manager_id))
        continue;

    $list[$item->id] = array(
        'name' => $item->title,
        'id' => $item->id
    );
}

$select = 'SELECT COUNT(id) as num FROM {user} as u ';
$record = $DB->get_record_sql($select . $where);

die(json_encode(array(
    'total_count' => $record->num,
    'items' => array_values($list)
)));

