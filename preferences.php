<?php

require_once("../../config.php");
require_once("lib.php");
require_once("classes/forms/talentreview_preferences_form.php");

require_login();

// managers redirect to employers page
if(!is_siteadmin()) {
    redirect(new moodle_url("/local/talentreview/"));
}

$context = context_system::instance();
$title = get_string('preferences', 'local_talentreview');

$PAGE->set_context(context_system::instance());
$PAGE->set_url(new moodle_url("/local/talentreview/index.php"));
$PAGE->requires->jquery();

$PAGE->navbar->add(get_string('pluginname', 'local_talentreview'), new moodle_url('/local/talentreview/'));
$PAGE->navbar->add($title);

$PAGE->set_title($title);
$PAGE->set_pagelayout('course');
$PAGE->set_heading($title);

$PAGE->requires->css('/local/talentreview/style.css', true);

$form = new talentreview_preferences_form(null, []);
$form_data = new stdClass();
$form_data->submitdate = get_config('local_talentreview', 'submitdate');
$form_data->lockdate = get_config('local_talentreview', 'lockdate');
$form_data->hideuntildate = get_config('local_talentreview', 'hideuntildate');
$form->set_data($form_data);

if ($form->is_cancelled()) {
    redirect(new moodle_url('/local/talentreview/managers.php'));
} else if ($data = $form->get_data()) {

    set_config('submitdate', $data->submitdate, 'local_talentreview');
    set_config('lockdate', $data->lockdate, 'local_talentreview');
    set_config('opendate', $data->lockdate, 'local_talentreview');
    set_config('hideuntildate', $data->hideuntildate, 'local_talentreview');
    redirect(new moodle_url('/local/talentreview/preferences.php'));
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo $form->display();

echo $OUTPUT->footer();
