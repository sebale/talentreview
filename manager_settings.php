<?php

require_once("../../config.php");
require_once("lib.php");
require_once("classes/tables/manager_settings_table.php");

require_login();

// managers redirect to employers page
if(!is_siteadmin()) {
    redirect(new moodle_url("/local/talentreview/"));
}

$action = optional_param('action', '', PARAM_TEXT);
$role_id = optional_param('id', 0, PARAM_INT);

if($role_id) {
    $status = false;

    if($action == 'enable') {
        $status = true;
    }

    if($action == 'disable') {
        $status = false;
    }

    $record = $DB->get_record('local_talentreview_managers', array('role_id' => $role_id));
    if($record && $record->role_id) {
        $record->status = $status;
        $DB->update_record('local_talentreview_managers', $record);
    } else {
        $record = new stdClass();
        $record->id = null;
        $record->role_id = $role_id;
        $record->status = $status;
        $DB->insert_record('local_talentreview_managers', $record, false);
    }
    redirect((new moodle_url($CFG->wwwroot . "/local/talentreview/manager_settings.php")));
}

$context = context_system::instance();
$title = 'Manager Settings';

$PAGE->set_context(context_system::instance());
$PAGE->set_url(new moodle_url($CFG->wwwroot . "/local/talentreview/manager_settings.php"));

$PAGE->set_title($title);
$PAGE->set_pagelayout('course');
$PAGE->set_heading($title);

$PAGE->navbar->add(get_string('pluginname', 'local_talentreview'), new moodle_url('/local/talentreview/'));
$PAGE->navbar->add($title);

$PAGE->requires->css('/local/talentreview/style.css', true);

$table = new manager_settings_table();
$table->show_download_buttons_at([]);
$table->is_collapsible = false;

echo $OUTPUT->header();

?>

<?php echo $OUTPUT->heading($title); ?>

<?php $table->out(20, true); ?>

<?php
echo $OUTPUT->footer();