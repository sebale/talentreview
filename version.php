<?php

$plugin->release   = '1.0';
$plugin->version  = 2017111300;    // The current module version (Date: YYYYMMDDXX).
$plugin->requires = 2014110400;    // Requires this Moodle version.
$plugin->maturity  = MATURITY_STABLE;
$plugin->component = 'local_talentreview';
$plugin->cron = 86400;
