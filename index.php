<?php

require_once("../../config.php");
require_once("lib.php");
require_once("classes/tables/learner_form_table.php");

require_login();

$context = context_system::instance();
require_capability('local/talentreview:index', $context);

// admin redirect to managers page
if(is_siteadmin()) {
    redirect(new moodle_url("/local/talentreview/managers.php"));
}

$view = optional_param('view', 1, PARAM_INT);
$download = optional_param('download', '', PARAM_ALPHA);

$title = get_string('pluginname', 'local_talentreview');

$PAGE->set_context(context_system::instance());
$PAGE->set_url(new moodle_url("/local/talentreview/index.php"));
$PAGE->requires->jquery();
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_pagelayout('course');
$PAGE->set_heading($title);

$PAGE->requires->css('/local/talentreview/style.css', true);

echo $OUTPUT->header();

$table = new learner_form_table($view);
$table->show_download_buttons_at([]);
$table->is_collapsible = false;

$manager_button = '';

if(LocalTalentReview::has_manager_access()) {
    /*$img = html_writer::tag('img', '', array(
        'src' => $CFG->wwwroot . '/local/talentreview/assets/img/hamburger.png',
        'height' => '15'
    ));*/

    $manager_button = html_writer::tag('a', 'Manage Forms', array(
        'href' => $CFG->wwwroot . '/local/talentreview/employees.php',
        'class' => 'btn my-form pull-right'
    ));
}

?>

    <div class="controls controls-row">
        <div class="span8">
            <?php echo $OUTPUT->heading($title); ?>
        </div>
        <div class="span4">
            <?php echo $manager_button . html_writer::tag('a', 'Self Review', [
                    'href' => $CFG->wwwroot . '/local/talentreview/form.php?id=0',
                    'class' => 'btn pull-right'
                ]); ?>
        </div>
    </div>

    <?php $table->out(20, true); ?>

<?php
echo $OUTPUT->footer();