<?php

global $CFG;

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->libdir . '/pear/HTML/QuickForm/element.php');

class talentreview_preferences_form extends moodleform {

    /**
     * Define the cohort edit form
     */
    public function definition() {
        $mform = $this->_form;

        $mform->addElement('date_selector', 'submitdate', get_string('submitdate', 'local_talentreview'), array('optional'=>true));
        $mform->addHelpButton('submitdate', 'submitdate', 'local_talentreview');
        $mform->setType('submitdate', PARAM_INT);

        $mform->addElement('date_selector', 'lockdate', get_string('lockdate', 'local_talentreview'), array('optional'=>true));
        $mform->addHelpButton('lockdate', 'lockdate', 'local_talentreview');
        $mform->setType('lockdate', PARAM_INT);

        $mform->addElement('date_selector', 'hideuntildate', get_string('hideuntildate', 'local_talentreview'), array('optional'=>true));
        $mform->addHelpButton('hideuntildate', 'hideuntildate', 'local_talentreview');
        $mform->setType('hideuntildate', PARAM_INT);

        $this->add_action_buttons();
    }
}
