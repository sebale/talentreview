<?php

global $CFG;

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->libdir . '/pear/HTML/QuickForm/element.php');

class talentreview_assign_form extends moodleform {

    /**
     * Define the cohort edit form
     */
    public function definition() {
        global $DB, $CFG;

        $mform = $this->_form;
        $manager_id = $this->_customdata['id'];

        $mform->addElement('select', 'users', '', array(), array(
            'multiple' => 'multiple',
        ));

        $mform->addElement('hidden', 'ids');
        $mform->setType('ids', PARAM_RAW);

        $mform->addElement('hidden', 'manager_id');
        $mform->setType('manager_id', PARAM_INT);
        $mform->setDefault('manager_id', $manager_id);

        $mform->addElement('html', '<input name="submitbutton" value="Assign" type="submit" id="id_submitbutton" style="margin-top: 7px;">');
    }
}