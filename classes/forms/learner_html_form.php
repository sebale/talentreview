<?php

global $CFG;

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->libdir . '/pear/HTML/QuickForm/element.php');

class learner_html_form extends moodleform {

    public function definition() {

        global $DB, $OUTPUT, $CFG, $USER;

        $strrequired = get_string('required');
        //$asterisk = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('req'), 'alt' => $strrequired, 'title' => $strrequired, 'class' => 'req'));
        $asterisk = '';

        $form = $this->_form; // Don't forget the underscore!
        $user_id = $this->_customdata['user_id'];
        $manager_id = $this->_customdata['manager_id'];
        $form_id = $this->_customdata['form_id'];
        $image_value = get_string('none');
        $data = $this->_customdata['data'];
        $context = context_system::instance();

        $form->addElement('hidden', 'id');
        $form->setType('id', PARAM_RAW);

        $form->addElement('hidden', 'user_id');
        $form->setType('user_id', PARAM_RAW);
        if(!$form_id) {
            $form->setDefault('user_id', $user_id);
        }

        $form->addElement('hidden', 'manager_id');
        $form->setType('manager_id', PARAM_RAW);
        if(!$form_id) {
            $form->setDefault('manager_id', $manager_id);
        }

        $form->addElement('hidden', 'approve');
        $form->setType('approve', PARAM_RAW);

        $form->addElement('hidden', 'copy');
        $form->setType('copy', PARAM_RAW);
        $form->setDefault('copy', 0);

        $form->addElement('hidden', 'photo');
        $form->setType('photo', PARAM_TEXT);

        $form->addElement('hidden', 'form_origin');
        $form->setType('form_origin', PARAM_TEXT);

        $form->addElement('hidden', 'review_date');
        $form->setType('review_date', PARAM_TEXT);

        $form->addElement('hidden', 'complited_date');
        $form->setType('complited_date', PARAM_TEXT);

        if(!LocalTalentReview::has_manager_access()) {
            $form->addElement('hidden', 'overal_review_rating');
            $form->setType('overal_review_rating', PARAM_TEXT);

            $form->addElement('hidden', 'overal_review_rating_previous');
            $form->setType('overal_review_rating_previous', PARAM_TEXT);
        }

        // dont change fix_fieldset_form
        $form->addElement('static', 'fix_fieldset_form', null);

        // block profile info
        $form->addElement('html', '<div class="controls controls-row">');

        $form->addElement('html', '<span>Profile Setting</span>');

        // block user picture
        $form->addElement('static', 'current_photo', get_string('currentpicture'));
        $user = $DB->get_record('user', array('id' => $user_id));

        if(!$form_id) {

            if ($user->picture) {
                $image_value = $OUTPUT->user_picture($user, [
                    'courseid' => SITEID,
                    'size' => 64
                ]);
            }

        } else {
            $form_data = $DB->get_record('local_talentreview', ['id' => $form_id]);

            $context = context_user::instance($user_id, MUST_EXIST);
            $fs = get_file_storage();

            $file = $fs->get_file_by_id($form_data->photo);
            if($file) {
                $fileurl = moodle_url::make_file_url('/pluginfile.php', '/' . $context->id . '/' . $file->get_component() . '/' . $file->get_filearea() . '/' . $form_data->photo);
                $src = $fileurl->__toString();
                $image_value = html_writer::tag('img', '', [
                    'src' => $src
                ]);
            }
        }

        $imagee_lement = $form->getElement('current_photo');
        $imagee_lement->setValue($image_value);

        $form->addElement('filemanager', 'imagefile', get_string('newpicture'), '', [
            'maxbytes'       => 1024*1024*10,
            'subdirs'        => 0,
            'maxfiles'       => 1,
            'accepted_types' => 'web_image'
        ]);

        $form->addHelpButton('imagefile', 'newpicture');
        // end block picture

        $form->addElement('html', '<hr>');

        $form->addElement('text', 'name', get_string('name', 'local_talentreview').$asterisk);
        $form->setType('name', PARAM_TEXT);

        $form->addElement('text', 'title', get_string('title', 'local_talentreview').$asterisk);
        $form->setType('title', PARAM_TEXT);

        $form->addElement('text', 'bu_function', get_string('bu_function', 'local_talentreview').$asterisk);
        $form->setType('bu_function', PARAM_TEXT);

        $form->addElement('html', '<hr>');

        $form->addElement('text', 'manager', get_string('manager', 'local_talentreview').$asterisk);
        $form->setType('manager', PARAM_TEXT);

        $form->addElement('date_selector', 'svmhiredate', get_string('svmhiredate', 'local_talentreview').$asterisk);
        $form->setType('svmhiredate', PARAM_INT);

        /*$form->addElement('text', 'timeinrole', get_string('timeinrole', 'local_talentreview').$asterisk);
        $form->setType('timeinrole', PARAM_TEXT);*/

        $form->addElement('html', '<hr>');

        $form->addElement('text', 'jde_eeid', get_string('jde_eeid', 'local_talentreview').$asterisk);
        $form->setType('jde_eeid', PARAM_TEXT);

        $form->addElement('html', '<hr>');

        $options = array(''=>'', get_string('somehighschool', 'local_talentreview') => get_string('somehighschool', 'local_talentreview'), get_string('highschoolged', 'local_talentreview') => get_string('highschoolged', 'local_talentreview'), get_string('somecollege', 'local_talentreview') => get_string('somecollege', 'local_talentreview'), get_string('associates', 'local_talentreview') => get_string('associates', 'local_talentreview'), get_string('bachelors', 'local_talentreview') => get_string('bachelors', 'local_talentreview'), get_string('masters', 'local_talentreview') => get_string('masters', 'local_talentreview'), get_string('doctorate', 'local_talentreview') => get_string('doctorate', 'local_talentreview'));
        $form->addElement('select', 'degree', get_string('degree', 'local_talentreview').$asterisk, $options);
        $form->setType('degree', PARAM_RAW);

        $form->addElement('text', 'university_school', get_string('university_school', 'local_talentreview').$asterisk, array('maxlength' => 25));
        $form->setType('university_school', PARAM_TEXT);

        $form->addElement('text', 'previous_employer', get_string('previous_work', 'local_talentreview').$asterisk, array('maxlength' => 25));
        $form->setType('previous_employer', PARAM_TEXT);

        $form->addElement('html', '</div>');


        if (LocalTalentReview::has_manager_access()) {

            $form->addElement('html', '<div class="controls controls-row">');
            $form->addElement('html', '<span>'.get_string('overallreviewrating', 'local_talentreview').'</span>');

            $year_arr = [];
            foreach (array_fill(2016, 15, false) as $year => $null) {
                $year_arr[$year] = $year;
            }
            $form->addElement('select', 'review_year', get_string('review_year', 'local_talentreview').$asterisk, $year_arr);
            if (!isset($data->review_year) or empty($data->review_year)){
                $form->setDefault('review_year', date('Y'));
            }

            $form->addElement('select', 'overal_review_rating',
                'Review Rating'.$asterisk,
                [
                    ''  => '',
                    '1' => get_string('rating_1', 'local_talentreview'),
                    '2' => get_string('rating_2', 'local_talentreview'),
                    '3' => get_string('rating_3', 'local_talentreview'),
                    '4' => get_string('rating_4', 'local_talentreview'),
                    '5' => get_string('rating_5', 'local_talentreview')
                ]);

            $form->addElement('select', 'overal_review_rating_previous',
                'Review Rating Previous Year'.$asterisk,
                [
                    ''  => '',
                    '1' => get_string('rating_1', 'local_talentreview'),
                    '2' => get_string('rating_2', 'local_talentreview'),
                    '3' => get_string('rating_3', 'local_talentreview'),
                    '4' => get_string('rating_4', 'local_talentreview'),
                    '5' => get_string('rating_5', 'local_talentreview')
                ]);

            $form->addElement('html', '<span class="description">'.get_string('rating_req', 'local_talentreview').'</span>');

            $form->addElement('html', '</div>');
        } else {
            $form->addElement('hidden', 'review_year');
            $form->setType('review_year', PARAM_INT);
            if (!isset($data->review_year) or empty($data->review_year)){
                $form->setDefault('review_year', date('Y'));
            }

            $form->addElement('hidden', 'overal_review_rating');
            $form->setType('overal_review_rating', PARAM_INT);

            $form->addElement('hidden', 'overal_review_rating_previous');
            $form->setType('overal_review_rating_previous', PARAM_INT);
        }

        $form->addElement('html', '<div class="controls controls-row">');
        $form->addElement('html', '<span>Goals/Objectives (WHAT)</span>');
        $form->addElement('html', '<span class="help">N: Needs Development, A: At Standard, E: Exceeds</span>');
        $form->addElement('html', '<span class="description">Should be based on all goals that were set at the beginning of the year and the extent to which the goals were achieved.</span>');

        $form->addElement('select', 'goals_perfomance_overal',
            get_string('goals_perfomance_overal', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);
        $form->addElement('html', '</div>');

        $form->addElement('html', '<div class="controls controls-row">');
        $form->addElement('html', '<span>Goals/Objectives Comments</span>');
        $form->addElement('html', '<span class="description">'.get_string('goals_desc', 'local_talentreview').'</span>');

        $form->addElement('select', 'goals_rating',
            get_string('goal1_rating', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);

        $form->addElement('textarea', 'goals_comment',
            get_string('goal1_comment', 'local_talentreview').$asterisk,
            [
                'rows' => 5,
                'style' => 'width: 95%',
                'maxlength' => 385
            ]
        );

        $form->addElement('html', '<hr />');
        $form->addElement('select', 'goal2_rating',
            get_string('goal2_rating', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);

        $form->addElement('textarea', 'goal2_comment',
            get_string('goal2_comment', 'local_talentreview').$asterisk,
            [
                'rows' => 5,
                'style' => 'width: 95%',
                'maxlength' => 385
            ]
        );

        $form->addElement('html', '<hr />');
        $form->addElement('select', 'goal3_rating',
            get_string('goal3_rating', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);

        $form->addElement('textarea', 'goal3_comment',
            get_string('goal3_comment', 'local_talentreview').$asterisk,
            [
                'rows' => 5,
                'style' => 'width: 95%',
                'maxlength' => 385
            ]
        );

        $form->addElement('html', '<hr />');
        $form->addElement('select', 'goal4_rating',
            get_string('goal4_rating', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);

        $form->addElement('textarea', 'goal4_comment',
            get_string('goal4_comment', 'local_talentreview').$asterisk,
            [
                'rows' => 5,
                'style' => 'width: 95%',
                'maxlength' => 385
            ]
        );
        $form->addElement('html', '</div>');

        $form->addElement('html', '<div class="controls controls-row">');
        $form->addElement('html', '<span>Behaviors (HOW)</span>');
        $form->addElement('html', '<span class="help">N: Needs Development, A: At Standard, E: Exceeds</span>');
        $form->addElement('html', '<span class="description">'.get_string('behaviors_desc', 'local_talentreview').'</span>');

        $form->addElement('select', 'behaviors_growth',
            get_string('behaviors_growth', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);

        $form->addElement('select', 'behaviors_accountability',
            get_string('behaviors_accountability', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);

        $form->addElement('select', 'behaviors_self_aware',
            get_string('behaviors_self_aware', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);

        $form->addElement('select', 'behaviors_champions',
            get_string('behaviors_champions', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);

        $form->addElement('select', 'behaviors_initiative',
            get_string('behaviors_initiative', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);

        $form->addElement('select', 'behaviors_judgment',
            get_string('behaviors_judgment', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);

        $form->addElement('select', 'behaviors_makes_people',
            get_string('behaviors_makes_people', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);

        $form->addElement('select', 'behaviors_leadership',
            get_string('behaviors_leadership', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);

        $form->addElement('select', 'behaviors_effective_com',
            get_string('behaviors_effective_com', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);

        $form->addElement('select', 'behaviors_gets_result',
            get_string('behaviors_gets_result', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);

        $form->addElement('select', 'behaviors_integrative',
            get_string('behaviors_integrative', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);

        $form->addElement('select', 'behaviors_intelligent',
            get_string('behaviors_intelligent', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);

        $form->addElement('html', '<hr>');

        $form->addElement('select', 'behaviors_overal',
            get_string('behaviors_overal', 'local_talentreview').$asterisk,
            [
                '' => '',
                'N' => get_string('perfomance_n', 'local_talentreview'),
                'A' => get_string('perfomance_a', 'local_talentreview'),
                'E' => get_string('perfomance_e', 'local_talentreview'),
            ]);
        $form->addElement('html', '</div>');



        $form->addElement('html', '<div class="controls controls-row">');
        $form->addElement('html', '<span>'.get_string('strengths', 'local_talentreview').' <i>(List and describe 2 – 3 strengths)</i></span>');
        $form->addElement('html', '<span class="description">'.get_string('strengths_desc', 'local_talentreview').'</span>');

        $conf = form_conf('strengths');
        $form->addElement('textarea', 'strengths',
            get_string('strengths', 'local_talentreview').$asterisk,
            [
                'rows' => 6,
                'style' => 'width: 95%',
                'maxlength' => $conf['num_chars']
            ]
        );
        $form->addElement('html', '</div>');

        $form->addElement('html', '<div class="controls controls-row">');
        $form->addElement('html', '<span>'.get_string('developmentneeds', 'local_talentreview').' <i>(List and describe 2 – 3 Needs Developmentelopment)</i></span>');
        $form->addElement('html', html_writer::tag('span', get_string('developmentneeds_desc', 'local_talentreview'), array('class' => 'description')));

        $conf = form_conf('development_needs');
        $form->addElement('textarea', 'development_needs',
            get_string('development_needs', 'local_talentreview').$asterisk,
            [
                'rows' => 6,
                'style' => 'width: 95%',
                'maxlength' => $conf['num_chars']
            ]
        );
        $form->addElement('html', '</div>');


        $form->addElement('html', '<div class="controls controls-row promotability-block">');
        $form->addElement('html', '<span>' . get_string('promotability', 'local_talentreview') . '</span>');
        $form->addElement('html', '<span class="help">Place an X to designate potential</span>');
        $html = html_writer::start_tag('ul', array('class' => 'promotability-ul'));
        $html .= html_writer::tag('li', get_string('promotable2_desc', 'local_talentreview'));
        $html .= html_writer::tag('li', get_string('promotable1_desc', 'local_talentreview'));
        $html .= html_writer::tag('li', get_string('trusted_desc', 'local_talentreview'));
        $html .= html_writer::tag('li', get_string('placement_desc', 'local_talentreview'));
        $html .= html_writer::tag('li', get_string('toonew_desc', 'local_talentreview'));
        $html .= html_writer::end_tag('ul');
        $form->addElement('html', html_writer::tag('span', $html, array('class' => 'description')));

        $form->addElement('checkbox', 'promotability_hp2', get_string('promotability_hp2', 'local_talentreview'));
        $form->addElement('checkbox', 'promotability_hp1', get_string('promotability_hp1', 'local_talentreview'));
        $form->addElement('checkbox', 'promotability_trusted', get_string('promotability_trusted', 'local_talentreview'));
        $form->addElement('checkbox', 'promotability_placement', get_string('promotability_placement', 'local_talentreview'));
        $form->addElement('checkbox', 'promotability_too_new', get_string('promotability_too_new', 'local_talentreview'));

        $form->addElement('html', '</div>');

        $form->addElement('html', '<div class="controls controls-row">');
        $form->addElement('html', '<span>' . get_string('mobility', 'local_talentreview') . '</span>');

        $relocate_arr = [];
        $relocate_arr[] = $form->createElement('radio', 'relocatability', '', get_string('yes'), 1);
        $relocate_arr[] = $form->createElement('radio', 'relocatability', '', get_string('no'), 0);
        $form->addGroup($relocate_arr, 'radio_group', get_string('relocatability_group_label', 'local_talentreview').$asterisk, ' ', false);

        $form->addElement('html', '</div>');

        $form->addElement('html', '<div class="controls controls-row">');
        $form->addElement('html', '<span>' . get_string('next_possible_move', 'local_talentreview') . '</span>');
        $form->addElement('html', '<span class="description">Considering the employee’s career goals, indicate the next role that would be developmentally beneficial. </span>');

        $conf = form_conf('next_possible_move');
        $form->addElement('text', 'next_possible_move',
            get_string('comments', 'local_talentreview').$asterisk,
            [
                /*'rows' => 6,*/
                'style' => 'width: 95%',
                'maxlength' => $conf['num_chars']
            ]
        );
        $form->addElement('html', '</div>');

        $form->addElement('html', '<div class="controls controls-row">');
        $form->addElement('html', '<span>' . get_string('development_plan', 'local_talentreview') . '</span>');
        $form->addElement('html', '<span class="description">Summarize the employee’s total performance over the past year.  Also include what you think the employee should focus on for development moving forward. </span>');

        $conf = form_conf('development_plan');
        $form->addElement('textarea', 'development_plan',
            get_string('comments', 'local_talentreview').$asterisk,
            [
                'rows' => 6,
                'style' => 'width: 95%',
                'maxlength' => $conf['num_chars']
            ]
        );
        $form->addElement('html', '</div>');

        if ($record->user_id != $USER->id and $user_id != $USER->id) {
            $form->addElement('html', '<hr>');

            $form->addElement('html', '<span class="description">'.get_string('manager_signature_desc', 'local_talentreview').'</span><br /><br />');

            $form->addElement('text', 'manager_signature', get_string('manager_signature', 'local_talentreview').$asterisk);
            $form->setType('manager_signature', PARAM_TEXT);

            $form->addElement('html', '<hr>');

        } else {
            $form->addElement('html', '<hr>');

            $form->addElement('text', 'signature', get_string('signature', 'local_talentreview').$asterisk);
            $form->setType('manager', PARAM_TEXT);

            $form->addElement('html', '<hr>');
        }

        $action_buttons = '<div id="fgroup_id_buttonar" class="fitem fitem_actionbuttons fitem_fgroup">
                                <div class="felement fgroup">';

        if(!$_REQUEST['id'] /*&& $user_id == $USER->id*/) {
            $action_buttons .= '<input name="save_button" value="Save" type="submit" id="id_savebutton">';
        } else {
            $record = $DB->get_record('local_talentreview', array('id' => $_REQUEST['id']), 'user_id, has_save');
            if ($record && ($record->has_save != LocalTalentReview::$SAVE_STATUS_SUBMIT or ($record->has_save == LocalTalentReview::$SAVE_STATUS_SUBMIT and $record->user_id != $USER->id))) {
                $action_buttons .= '<input name="save_button" value="Save" type="submit" id="id_savebutton">';
            }
        }

        if (!$data->copy) {
            if ($record->user_id != $USER->id) {
                $action_buttons .= '<input name="submitbutton" onclick="return confirm(\''.get_string('submitconfirm', 'local_talentreview').'\')" value="Submit" type="submit" id="id_submitbutton">';
            } else {
                $action_buttons .= '<input name="submitbutton" value="Submit" type="submit" id="id_submitbutton">';
            }
        }
                $action_buttons .= '<input name="cancel" value="Cancel" type="submit" onclick="skipClientValidation = true; return true;" class=" btn-cancel" id="id_cancel">
                                </div>
                            </div>';

        $form->addElement('html', $action_buttons);
        //$this->add_action_buttons(true, 'Submit');
    }

    function validation($data, $files) {
        global $CFG, $DB, $USER;

        $errors = array();
        $data = (object)$data;

        if (isset($_REQUEST['submitbutton'])) {
            if (empty($data->name)) {
                $errors['name'] = get_string('required');
            }
            if (empty($data->title)) {
                $errors['title'] = get_string('required');
            }
            if (empty($data->bu_function)) {
                $errors['bu_function'] = get_string('required');
            }
            if (empty($data->manager)) {
                $errors['manager'] = get_string('required');
            }
            if (empty($data->svmhiredate)) {
                $errors['svmhiredate'] = get_string('required');
            }
            /*if (empty($data->timeinrole)) {
                $errors['timeinrole'] = get_string('required');
            }*/
            if (empty($data->jde_eeid)) {
                $errors['jde_eeid'] = get_string('required');
            }
            if (empty($data->degree)) {
                $errors['degree'] = get_string('required');
            }
            if (empty($data->university_school)) {
                $errors['university_school'] = get_string('required');
            }
            if (empty($data->previous_employer)) {
                $errors['previous_employer'] = get_string('required');
            }
            if (LocalTalentReview::has_manager_access()) {
                if (empty($data->review_year)) {
                    $errors['review_year'] = get_string('required');
                }
                if (empty($data->overal_review_rating)) {
                    $errors['overal_review_rating'] = get_string('required');
                }
                if (empty($data->overal_review_rating_previous)) {
                    $errors['overal_review_rating_previous'] = get_string('required');
                }
            }
            /*if (empty($data->goals_perfomance_overal)) {
                $errors['goals_perfomance_overal'] = get_string('required');
            }
            if (empty($data->goals_rating)) {
                $errors['goals_rating'] = get_string('required');
            }*/
            if (empty($data->goals_comment)) {
                $errors['goals_comment'] = get_string('required');
            }
            /*if (empty($data->goal2_rating)) {
                $errors['goal2_rating'] = get_string('required');
            }*/
            if (empty($data->goal2_comment)) {
                $errors['goal2_comment'] = get_string('required');
            }
            /*if (empty($data->goal3_rating)) {
                $errors['goal3_rating'] = get_string('required');
            }*/
            if (empty($data->goal3_comment)) {
                $errors['goal3_comment'] = get_string('required');
            }
            /*if (empty($data->goal4_rating)) {
                $errors['goal4_rating'] = get_string('required');
            }*/
            if (empty($data->goal4_comment)) {
                $errors['goal4_comment'] = get_string('required');
            }
            /*if (empty($data->behaviors_overal)) {
                $errors['behaviors_overal'] = get_string('required');
            }*/
            if (empty($data->strengths)) {
                $errors['strengths'] = get_string('required');
            }
            if (empty($data->development_needs)) {
                $errors['development_needs'] = get_string('required');
            }
            if (!isset($data->relocatability)) {
                $errors['relocatability'] = get_string('required');
            }
            if (empty($data->next_possible_move)) {
                $errors['next_possible_move'] = get_string('required');
            }
            if (empty($data->development_plan)) {
                $errors['development_plan'] = get_string('required');
            }
            if (!isset($data->promotability_hp2) and
                !isset($data->promotability_hp1) and
                !isset($data->promotability_trusted) and
                !isset($data->promotability_placement) and
                !isset($data->promotability_too_new)) {
                $errors['promotability_hp2'] = get_string('required');
            }
            if (isset($data->signature) and empty($data->signature)) {
                $errors['signature'] = get_string('required');
            }

            if (isset($data->manager_signature) and empty($data->manager_signature)) {
                $errors['manager_signature'] = get_string('required');
            }

            $behaviours = 0;
            $behaviours += ($data->behaviors_growth == 'N') ? 1 : 0;
            $behaviours += ($data->behaviors_accountability == 'N') ? 1 : 0;
            $behaviours += ($data->behaviors_self_aware == 'N') ? 1 : 0;
            $behaviours += ($data->behaviors_champions == 'N') ? 1 : 0;
            $behaviours += ($data->behaviors_initiative == 'N') ? 1 : 0;
            $behaviours += ($data->behaviors_judgment == 'N') ? 1 : 0;
            $behaviours += ($data->behaviors_makes_people == 'N') ? 1 : 0;
            $behaviours += ($data->behaviors_leadership == 'N') ? 1 : 0;
            $behaviours += ($data->behaviors_effective_com == 'N') ? 1 : 0;
            $behaviours += ($data->behaviors_gets_result == 'N') ? 1 : 0;
            $behaviours += ($data->behaviors_integrative == 'N') ? 1 : 0;
            $behaviours += ($data->behaviors_intelligent == 'N') ? 1 : 0;

            if ($behaviours < 2) {
                $errors['behaviors_growth'] = get_string('behaviorrequiren', 'local_talentreview');
            }
        }

        return $errors;
    }
}

