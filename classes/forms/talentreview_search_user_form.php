<?php

global $CFG;

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->libdir . '/pear/HTML/QuickForm/element.php');

class talentreview_search_user_form extends moodleform {

    /**
     * Define the cohort edit form
     */
    public function definition() {
        global $DB, $CFG;

        $mform = $this->_form;
        $print = (isset($this->_customdata['print'])) ? $this->_customdata['print'] : 0;
        $mform->_attributes['class'] .= ' trw-search-form'.(($print) ? ' with-print' : '');

        $statusfilter = get_user_preferences('statusfilter', 0);
        $mform->addElement('select', 'statusfilter',
            get_string('status', 'local_talentreview'),
            array(
                '-1' => get_string('showall', 'local_talentreview'),
                '0' => get_string('active', 'local_talentreview'),
                '1' => get_string('inactive', 'local_talentreview'),
            ));
        $mform->setDefault('statusfilter', $statusfilter);

        $mform->addElement('text', 'search', '', array('placeholder' => 'Search...'));
        $mform->setType('search', PARAM_RAW);


        // print actions
        if ($print) {
            $mform->addElement('html', '<input name="print" value="Print" type="button" onclick="$(\'#bulk_print_form\').submit();">');
        }

        $mform->addElement('html', '<input name="submitbutton" value="Search" type="submit">');

    }
}
