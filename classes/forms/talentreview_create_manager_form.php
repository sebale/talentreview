<?php

global $CFG;

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->libdir . '/pear/HTML/QuickForm/element.php');

class talentreview_create_manager_form extends moodleform {

    /**
     * Define the cohort edit form
     */
    public function definition() {
        global $DB, $USER;

        $mform = $this->_form;
        $mform->_attributes['class'] .= ' trw-create-form';

        $options = [];
        $defaults = [];

        $users = array(); $ids = array();
        local_talentreview_get_users(array($USER->id), $users, $ids);

        if($users) {
            foreach ($users as $user) {
                if ($user->suspended) continue;
                $options[$user->id] = fullname($user);
            }
            asort($options);
        }

        $mform->addElement('select', 'to_user', '', $options);
        $mform->setDefault('users', $defaults);

        $mform->addElement('hidden', 'manager_id');
        $mform->setType('manager_id', PARAM_INT);
        $mform->setDefault('manager_id', $USER->id);

        $mform->addElement('html', '<input name="submitbutton" value="' . get_string('create_form', 'local_talentreview') . '" type="submit" id="id_submitbutton">');
    }
}
