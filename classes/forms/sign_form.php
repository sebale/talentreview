<?php

global $CFG;

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->libdir . '/pear/HTML/QuickForm/element.php');

class sign_form extends moodleform {
    function definition () {
        $mform = $this->_form;

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $mform->addElement('text', 'signature', get_string('signature', 'local_talentreview'));
        $mform->addRule('signature', get_string('required'), 'required', null, 'client');
        $mform->setType('manager', PARAM_TEXT);

        $mform->addElement('date_time_selector', 'signature_date', get_string('signature_date', 'local_talentreview'));
        $mform->addRule('signature_date', get_string('required'), 'required', null, 'client');
        $mform->setType('signature_date', PARAM_INT);

        $this->add_action_buttons(false, get_string('signform', 'local_talentreview'));
    }
}
