<?php

global $CFG;

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->libdir . '/pear/HTML/QuickForm/element.php');

class local_talentreview_notification_setting extends moodleform {

    /**
     * Define the cohort edit form
     */
    public function definition() {
        $mform = $this->_form;

        $mform->addElement('html', html_writer::tag('h5', 'Notification Sendler'));
        $mform->addElement('checkbox', 'status', 'Enable/Disable Notifications');
        $mform->addElement('text', 'notification_sendler', 'E-Mail');
        $mform->setType('notification_sendler', PARAM_EMAIL);

        $mform->addElement('html', '<hr>');

        $mform->addElement('html', html_writer::tag('h5', 'User Notification'));
        $mform->addElement('text', 'user_subject', 'Subject');
        $mform->setType('user_subject', PARAM_RAW);

        $mform->addElement('textarea', 'user_message', 'Message', array('rows' => 6, 'style' => 'width: 100%'));
        $mform->setType('user_message', PARAM_RAW);

        $mform->addElement('html', '<hr>');

        $mform->addElement('html', html_writer::tag('h5', 'Manager Notification'));
        $mform->addElement('text', 'manager_subject', 'Subject');
        $mform->setType('manager_subject', PARAM_RAW);

        $mform->addElement('textarea', 'manager_message', 'Message', array('rows' => 6, 'style' => 'width: 100%'));
        $mform->addElement('html', '<ul class="nav"><li><b>Mail tags</b></li><li>%user% - User name</li><li>%manager% - Manager name</li></ul>'); //<li>%url% - Link to form</li>
        $this->add_action_buttons();
    }
}