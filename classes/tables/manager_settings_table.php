<?php

global $CFG;
require_once($CFG->libdir . '/tablelib.php');

class manager_settings_table extends table_sql {
    function __construct() {
        global $CFG, $PAGE, $USER, $DB;

        parent::__construct('table');

        $columns = [];
        $headers = [];

        $columns[] = 'role';
        $headers[] = 'Role';

        $columns[] = 'status';
        $headers[] = 'Status';

        $columns[] = 'actions';
        $headers[] = 'Actions';

        $this->define_headers($headers);
        $this->define_columns($columns);

        $this->no_sorting('role');
        $this->no_sorting('status');
        $this->no_sorting('actions');

        //get_assignable_roles
        $context = context_system::instance();
        $assignable_roles = get_assignable_roles($context);

        $fields = "r.id, r.shortname as role, s.status, '' as actions";
        $from = '{role} r ';
        $from .= 'LEFT JOIN {local_talentreview_managers} as s ON r.id = s.role_id';
        $where = 'r.id IN ("' . ($assignable_roles ? implode('","', array_flip($assignable_roles)) : 0) . '") ';
        $where .= " ORDER BY r.shortname DESC";

        $this->set_sql($fields, $from, $where);
        $this->define_baseurl($PAGE->url);
    }

    function col_actions($values) {
        global $CFG, $OUTPUT;

        $buttons = [];
        $urlparams = ['id' => $values->id];

        if(!$values->status) {
            $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/talentreview/manager_settings.php', $urlparams + array('action' => 'enable')),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/approve'), 'alt' => '', 'class' => 'iconsmall')),
                array('title' => 'Enable'));
        } else {
            $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/talentreview/manager_settings.php', $urlparams + array('action' => 'disable')),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => '', 'class' => 'iconsmall')),
                array('title' => 'Disable'));
        }

        return implode(' ', $buttons);
    }

    function col_role($values) {
        return $values->role;
    }

    function col_status($values) {
        global $CFG, $OUTPUT;
        if($values->status) {
            return html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/hide'), 'class' => 'iconsmall'));
        } else {
            return html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/show'), 'class' => 'iconsmall'));
        }
    }

}