<?php

global $CFG;
require_once($CFG->libdir . '/tablelib.php');

class managers_table extends table_sql {
    function __construct($uniqueid, $search = '') {
        global $CFG, $PAGE, $USER, $DB;

        parent::__construct('table');

        $columns = [];
        $headers = [];

        $columns[] = 'firstname';
        $headers[] = get_string('user_name', 'local_talentreview');

        $columns[] = 'username';
        $headers[] = get_string('login', 'local_talentreview');

        $columns[] = 'role';
        $headers[] = 'System Role';

        $columns[] = 'assigned';
        $headers[] = 'Assigned Users';

        $columns[] = 'email';
        $headers[] = get_string('email', 'local_talentreview');

        $columns[] = 'lastlogin';
        $headers[] = get_string('lastlogin', 'local_talentreview');

        /*$columns[] = 'actions';
        $headers[] = get_string('actions', 'local_talentreview');*/

        $this->define_headers($headers);
        $this->define_columns($columns);

        $this->no_sorting('role');
        $this->no_sorting('assigned');

        // mdl_pos_assignment
        //
        $where = '1';

        if (!empty($search)) {
            $where .= " AND (u.firstname LIKE '%$search%' OR u.lastname LIKE '%$search%')";
        }

        $statusfilter = get_user_preferences('statusfilter', 0);
        if ($statusfilter >= 0 and $statusfilter < 2) {
            $where .= ' AND u.suspended = '.$statusfilter;
        }

        $fields = "u.id, u.email, u.firstname, u.lastname, u.username, u.lastlogin, u.suspended, '' as actions";
        $from = "{user} u INNER JOIN {pos_assignment} as pa ON pa.managerid = u.id";
        //'ra.roleid IN (SELECT role_id as id FROM {local_talentreview_managers} WHERE status = 1)';

        $this->set_sql($fields, $from, $where);
        $this->define_baseurl($PAGE->url);
    }

    function col_username($values) {
        return $values->username;
    }

    function col_role($values) {
        $col_data = '';
        foreach (LocalTalentReview::get_user_roles_assignment($values->id) as $obj) {
            $col_data .= !$col_data ? $obj->title : ('<br>' . $obj->title);
        }
        return $col_data;
    }

    function col_assigned($values) {
        return LocalTalentReview::getAssignedUsersCount($values->id);
    }

    function col_firstname($values) {
        return $values->firstname . ' ' . $values->lastname;
    }

    function col_lastlogin($values) {
        return ($values->lastlogin) ? date('m-d-Y h:i', $values->lastlogin) : '-';
    }

}
