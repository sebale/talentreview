<?php

global $CFG;
require_once($CFG->libdir . '/tablelib.php');

class assigned_users_table extends table_sql {
    function __construct($view = 1) {
        global $CFG, $PAGE, $USER, $DB;

        parent::__construct('table');

        $columns = [];
        $headers = [];

        $columns[] = 'username';
        $headers[] = get_string('user_name', 'local_talentreview');

        $columns[] = 'role';
        $headers[] = 'System Role';

        $columns[] = 'assigned';
        $headers[] = 'Assigned Users';

        $columns[] = 'email';
        $headers[] = get_string('email', 'local_talentreview');

        $columns[] = 'assigned_date';
        $headers[] = get_string('assigned', 'local_talentreview');

        $columns[] = 'actions';
        $headers[] = get_string('actions', 'local_talentreview');

        $this->define_headers($headers);
        $this->define_columns($columns);

        $this->no_sorting('role');
        $this->no_sorting('assigned');

        $fields = "u.id, u.confirmed, u.confirmed, u.email, u.firstname, u.lastname, assign.timemodified as created, '' as actions";
        $from = " {user} u LEFT JOIN {pos_assignment} assign ON assign.userid = u.id ";
        $where =  'assign.managerid = ' . optional_param('id', 0, PARAM_INT);

        $this->set_sql($fields, $from, $where, array());
        //$this->set_control_variables($users);
        $this->define_baseurl($PAGE->url);
    }

    function col_actions($values) {
        global $CFG, $OUTPUT;

        $buttons = [];
        $urlparams = [
            'id' => optional_param('id', 0, PARAM_INT),
            'user_id' => $values->id
        ];

        $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/talentreview/assignusers.php', $urlparams + array('action' => 'delete')),
            html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => 'Delete', 'class' => 'iconsmall')),
            array('title' => 'Delete'));

        return implode(' ', $buttons);
    }


    function col_username($values) {
        return $values->firstname . ' ' . $values->lastname;
    }

    function col_role($values) {
        $col_data = '';
        foreach (LocalTalentReview::get_user_roles_assignment($values->id) as $obj) {
            $col_data .= !$col_data ? $obj->title : ('<br>' . $obj->title);
        }
        return $col_data;
    }

    function col_assigned($values) {
        return LocalTalentReview::getAssignedUsersCount($values->id);
    }

    function col_assigned_date($values) {
        return date('m-d-Y h:i', $values->created);
    }

    function col_email($values) {
        return $values->email;
    }
}
