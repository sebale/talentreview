<?php

global $CFG;
require_once($CFG->libdir . '/tablelib.php');

class learner_form_table extends table_sql {
    function __construct($view = 1) {
        global $CFG, $PAGE, $USER;

        parent::__construct('table');

        $columns = [];
        $headers = [];

        $columns[] = 'review_year';
        $headers[] = get_string('review_year', 'local_talentreview');

        $columns[] = 'title';
        $headers[] = get_string('form_title', 'local_talentreview');

        $columns[] = 'approve';
        $headers[] = 'Status';

        $columns[] = 'form_origin';
        $headers[] = 'Form Origin';

        $columns[] = 'complited_date';
        $headers[] = get_string('complited_date', 'local_talentreview');

        $columns[] = 'review_date';
        $headers[] = get_string('review_date', 'local_talentreview');

        $columns[] = 'actions';
        $headers[] = get_string('actions', 'local_talentreview');

        $this->define_headers($headers);
        $this->define_columns($columns);

        $submittade = get_config('local_talentreview', 'submitdate');

        $fields = "f.*, '' as actions";
        $from = "{local_talentreview} f";

        $params = array('form_origin'=>'self', 'userid1'=>$USER->id, 'submitted'=>LocalTalentReview::$SAVE_STATUS_SUBMIT);

        if ($submittade > time()) {
            $where = 'f.user_id = :userid1 AND f.form_origin = :form_origin ';
        } else {
            $where = 'f.user_id = :userid1 AND (f.form_origin = :form_origin OR f.has_save = :submitted OR f.approve > 0) ';
        }

        // hide form to employee when hideuntildate setup
        $hidedate = get_config('local_talentreview', 'hideuntildate');
        if ($hidedate and $hidedate > time()) {
            $where .= ' AND f.id = 0 ';
        }

        //AND f.form_origin ="' . LocalTalentReview::$FORM_ORIGIN_SELF . '"

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl($PAGE->url);
    }

    function col_actions($values) {
        global $CFG, $OUTPUT;

        $buttons = [];
        $urlparams = ['id' => $values->id];

        $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/talentreview/form.php', $urlparams + array('action' => 'print')),
            html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('a/download_all'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
            array('title' => get_string('download_pdf', 'local_talentreview')));

        if(!$values->approve) {
            $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/talentreview/form.php', $urlparams),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'alt' => get_string('edit'), 'class' => 'iconsmall')),
                ['title' => get_string('edit')]);

            $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot . '/local/talentreview/form.php', $urlparams + ['action' => 'delete']),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
                array('title' => get_string('delete')));
        } elseif (empty($values->signature)) {
            $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/talentreview/sign.php', $urlparams),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'alt' => get_string('signform', 'local_talentreview'), 'class' => 'iconsmall')),
                ['title' => get_string('signform', 'local_talentreview')]);
        }
        return implode(' ', $buttons);
    }

    function col_title($values) {
        return $values->title;
    }

    function col_complited_date($values) {
        return $values->complited_date ? date('m-d-Y', $values->complited_date) : get_string('notset', 'local_talentreview');
    }

   function col_review_year($values) {
        return (!empty($values->review_year)) ? $values->review_year : get_string('notset', 'local_talentreview');
    }

    function col_review_date($values) {
        return $values->review_date ? date('m-d-Y', $values->review_date) : get_string('notset', 'local_talentreview');
    }

    function col_approve($values) {
        global $OUTPUT;
        if($values->approve) {
            return html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/approve'), 'class' => 'iconsmall'));
        } else {
            return html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'class' => 'iconsmall'));
        }
    }

    function col_form_origin($values) {
        return $values->form_origin;
    }
}
