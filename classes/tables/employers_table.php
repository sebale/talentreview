<?php

global $CFG;
require_once($CFG->libdir . '/tablelib.php');

class employers_table extends table_sql {
    private $debug_employer = null;

    function __construct() {
        global $PAGE, $USER;

        parent::__construct('employers_table');

        $columns = [];
        $headers = [];

        $columns[] = 'checkbox';
        $headers[] = '';

        $columns[] = 'review_year';
        $headers[] = get_string('review_year', 'local_talentreview');

        $columns[] = 'name';
        $headers[] = get_string('user_name', 'local_talentreview');

        $columns[] = 'title';
        $headers[] = get_string('title', 'local_talentreview');

        $columns[] = 'approve';
        $headers[] = 'Status';

        $columns[] = 'form_origin';
        $headers[] = 'Form Origin';

        $columns[] = 'complited_date';
        $headers[] = get_string('complited_date', 'local_talentreview');

        $columns[] = 'review_date';
        $headers[] = get_string('review_date', 'local_talentreview');

        // recursive search employees

        $users = array(); $ids = array();
        local_talentreview_get_users(array($USER->id), $users, $ids);

        if($this->debug_employer >= 2) {
            $columns[] = 'manager';
            $headers[] = get_string('manager', 'local_talentreview');
        }

        $columns[] = 'actions';
        $headers[] = get_string('actions', 'local_talentreview');

        $this->define_headers($headers);
        $this->define_columns($columns);
        $this->no_sorting('checkbox');
        $this->no_sorting('actions');

        $search = optional_param('search', 0, PARAM_RAW);
        $where = 'f.user_id IN ("' . ($users ? implode('","', array_keys($users)) : 0) . '")';
        $where .= ' AND (f.has_save IN (' . LocalTalentReview::$SAVE_STATUS_DEFAULT . ', ' . LocalTalentReview::$SAVE_STATUS_SUBMIT . ') OR ((f.has_save = :saved AND f.form_origin <> :form_origin) OR f.approve > 0))';

        if($search) {
            $where .= ' AND f.name LIKE "%' . $search . '%"';
        }

        $statusfilter = get_user_preferences('statusfilter', 0);
        if ($statusfilter >= 0 and $statusfilter < 2) {
            $where .= ' AND u.suspended = '.$statusfilter;
        }

        $fields = "f.*, '' as actions, '' as checkbox";
        $from = "{local_talentreview} f LEFT JOIN {user} u ON u.id = f.user_id";

        $this->set_sql($fields, $from, $where, array('saved'=>LocalTalentReview::$SAVE_STATUS_SAVE, 'form_origin'=>'self'));
        $this->define_baseurl($PAGE->url);
    }

    function getEmployees($user_id) {
        global $DB;
        $employees = array();
        if($records = $DB->get_records('pos_assignment', array('managerid' => $user_id))) {
            $this->debug_employer++;
            foreach ($records as $record) {
                $employees = array_merge($employees, $this->getEmployees($record->userid));
                $employees[] = $record->userid;
            }
        }
        return $employees;
    }

    function col_checkbox($values){
        return html_writer::empty_tag("input", array('type'=>'checkbox', 'name'=> 'forms['.$values->id.']', 'class'=>'flextable-checkbox', 'value'=>'1'));
    }

    function col_manager($values) {
        return $values->manager;
    }

    function col_name($values) {
        return $values->name;
    }

    function col_title($values) {
        return $values->title;
    }

    function col_approve($values) {
        global $OUTPUT;
        if($values->approve) {
            return html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/approve'), 'class' => 'iconsmall'));
        } else {
            return html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'class' => 'iconsmall'));
        }
    }

    function col_form_origin($values) {
        return $values->form_origin;
    }

    function col_complited_date($values) {
        return $values->complited_date ? date('m-d-Y', $values->complited_date) : get_string('notset', 'local_talentreview');
    }

    function col_review_year($values) {
        return (!empty($values->review_year)) ? $values->review_year : get_string('notset', 'local_talentreview');
    }

    function col_review_date($values) {
        return ($values->review_date) ? date('m-d-Y', $values->review_date) : get_string('notset', 'local_talentreview');
    }

    function col_actions($values) {
        global $CFG, $OUTPUT;

        $buttons = [];
        $urlparams = ['id' => $values->id];

        $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/talentreview/form.php', $urlparams + array('action' => 'print')),
            html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('a/download_all'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
            array('title' => get_string('download_pdf', 'local_talentreview'), 'target'=>'_blank'));

        $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/talentreview/form.php', $urlparams + ['action' => 'copy']),
            html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/calc'), 'alt' => get_string('edit'), 'class' => 'iconsmall')),
            ['title' => get_string('copy_form', 'local_talentreview')]);

        if($values->approve) {
            /*$buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/talentreview/form.php', $urlparams + ['action' => 'block']),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/block'), 'alt' => 'Disapprove', 'class' => 'iconsmall')),
                array('title' => 'Disapprove'));*/
        } else {
            $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/talentreview/form.php', $urlparams + ['action' => 'approve']),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/approve'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
                array('title' => get_string('approve')));
        }

        $lockdate = get_config('local_talentreview', 'lockdate');
        if (($lockdate > 0 and $lockdate > time()) or !$lockdate or is_siteadmin()) {
            $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/talentreview/form.php', $urlparams),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'alt' => get_string('edit'), 'class' => 'iconsmall')),
                ['title' => get_string('edit')]);
        }

        if (is_siteadmin()) {
            $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/talentreview/form.php', $urlparams + ['action' => 'delete']),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
                array('title' => get_string('delete'), 'onclick' => "if (!confirm('".get_string('deletemessage', 'local_talentreview')."')) return false;"));
        }

        return implode(' ', $buttons);
    }
}
